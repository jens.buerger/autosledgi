/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.api.security

import com.autosledgi.job.resource.JobResource
import com.autosledgi.job.resource.JobResourceRepository
import com.autosledgi.job.system.SystemJob
import com.autosledgi.job.system.SystemJobFacade
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException
import java.security.Principal

@Component
class SecureRepository(
    private val systemJobFacade: SystemJobFacade,
    private val resourceRepository: JobResourceRepository
) {

    fun safelyGetSystemJob(jobId: Long, principal: Principal?): SystemJob {
        val searchResult = systemJobFacade.getJobById(jobId)
        val job = if (searchResult.isPresent) {
            searchResult.get()
        } else {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Invalid job # $jobId")
        }

        if (job.userId != getUserId(principal)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "Access to job # $jobId denied.")
        }
        return job
    }


    fun safelyGetJobResource(resourceId: Long, principal: Principal?): JobResource {
        val searchResult = resourceRepository.findById(resourceId)
        val jobResource = if (searchResult.isPresent) {
            searchResult.get()
        } else {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Invalid resource # $resourceId")
        }

        if (jobResource.userId != getUserId(principal)) {
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "Access to job # $resourceId denied.")
        }
        return jobResource
    }
}