/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.api.security

import com.autosledgi.user.KeycloakUserId
import com.autosledgi.user.UserServiceLevel
import org.keycloak.KeycloakPrincipal
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken
import java.security.Principal


/**
 * Resolve the user service level from the principal
 *
 * @param principal The principal that issued the request.
 *
 * @throws Exception when the service level cannot be resolved.
 */
fun getServiceLevel(principal: Principal?): UserServiceLevel {
    val keycloakPrincipal = (principal as KeycloakAuthenticationToken).principal as KeycloakPrincipal<*>
    val token = keycloakPrincipal.keycloakSecurityContext.token
    // TODO make this service_level attribute configurable
    return UserServiceLevel.valueOf(token.otherClaims["service_level"] as String)
}


// TODO only works if keycloak configuration does not prefer username
fun getUserId(principal: Principal?): KeycloakUserId? = principal?.let { KeycloakUserId(it.name) }
