/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.api.rest

import com.autosledgi.exception.ResourceStorageException
import com.autosledgi.job.resource.JobResource
import com.autosledgi.job.resource.JobResourceFacade
import mu.KLogging
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.security.Principal


/**
 * Controller for work package uploads.
 *
 * @author Jens Buerger
 */
@RestController
@RequestMapping("/user/jobresource")
class JobResourceUploadController(
    private val jobResourceFacade: JobResourceFacade
) {


    @PostMapping("/upload")
    fun upload(
        @RequestParam("file") file: MultipartFile,
        redirectAttributes: RedirectAttributes,
        principal: Principal?
    ): JobResource? {
        var jobResource: JobResource? = null
        if (file.isEmpty) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload")
        } else {
            try {
                jobResource = jobResourceFacade.createResource(file.inputStream, principal)
                redirectAttributes.addFlashAttribute(
                    "message",
                    "You successfully uploaded '" + file.originalFilename + "'"
                )
            } catch (e: ResourceStorageException) {
                // TODO error handling
                throw e
            }
        }

        return jobResource
    }

    @GetMapping("/uploadStatus")
    fun uploadStatus(): String {
        return "uploadStatus"
    }

    companion object : KLogging()
}