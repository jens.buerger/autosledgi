/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.api.rest

import mu.KLogging
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.multipart.MultipartException
import org.springframework.web.servlet.mvc.support.RedirectAttributes


/**
 * REST Exception Handler.
 *
 * @author Jens Buerger
 */
@ControllerAdvice
class GlobalExceptionHandler {

    @ExceptionHandler(MultipartException::class)
    fun handleMultipartError(exception: MultipartException, redirectAttributes: RedirectAttributes): String {
        exception.cause?.let { redirectAttributes.addFlashAttribute("message", it) }
        logger.warn("Multipart Upload failed", exception)
        return "redirect:/user/jobresource/uploadStatus"
    }

    companion object : KLogging()

}