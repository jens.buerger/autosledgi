/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.api.rest

import com.autosledgi.api.security.SecureRepository
import com.autosledgi.job.system.SystemJob
import com.autosledgi.job.system.SystemJobFacade
import mu.KLogging
import org.springframework.web.bind.annotation.*
import java.security.Principal

/**
 * Controller for the theory completion API.
 *
 * @author Jens Buerger
 */
@RestController
@RequestMapping("/user/job")
class SystemJobController(
    private val systemJobFacade: SystemJobFacade,
    private val secureRepository: SecureRepository
) {

    /**
     * Get Details for a currently job.
     *
     * @param jobId The autosledgi job id that represents the processing job.
     */
    @GetMapping("/details")
    fun jobDetails(
        @RequestParam(value = "id", required = true) jobId: Long,
        principal: Principal?
    ): SystemJob {
        logger.debug { "Resolving state of autosledgi job with id $jobId" }
        return secureRepository.safelyGetSystemJob(jobId, principal)
    }


    /**
     * Launch the  the processing of a job resource
     *
     * @param jobId The autosledgi job id that should be executed.
     */
    @PostMapping("/launch/{jobId:[\\d]+}")
    fun prepareProcessing(
        @PathVariable jobId: Long,
        principal: Principal?
    ): SystemJob {
        logger.debug { "Launching new theory completion job # $jobId" }
        val job = secureRepository.safelyGetSystemJob(jobId, principal)
        systemJobFacade.launch(job)
        logger.debug { "Finished launching job # $jobId" }
        return secureRepository.safelyGetSystemJob(jobId, principal)
    }


    companion object : KLogging()
}

