/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.api.rest

import com.autosledgi.api.security.SecureRepository
import com.autosledgi.api.security.getServiceLevel
import com.autosledgi.api.security.getUserId
import com.autosledgi.isabelle.theory.TheoryProcessingFacade
import com.autosledgi.job.system.SystemJob
import com.autosledgi.job.system.jobwrapper.TheoryCompletionJobRequest
import mu.KLogging
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.security.Principal


/**
 * Controller for the theory completion API.
 *
 * @author Jens Buerger
 */
@RestController
@RequestMapping("/user/theoryprocessing")
class TheoryProcessingController(
    private val secureRepository: SecureRepository,
    private val processingFacade: TheoryProcessingFacade
) {


    /**
     * Prepare the processing of a job resource
     *
     * @param jobResourceId The upload id of the job resource file that should be processed.
     * @param mainTheoryPath The theory that is processed.
     */
    @PostMapping("/prepare/{jobResourceId:[\\d]+}/{mainTheoryPath}")
    fun prepareProcessing(
        @PathVariable jobResourceId: Long,
        @PathVariable mainTheoryPath: String,
        principal: Principal?
    ): SystemJob {
        logger.debug { "Preparing new theory completion job on resource #1 $jobResourceId" }
        val jobResource = secureRepository.safelyGetJobResource(jobResourceId, principal)
        // TODO also check if theory file is contained in zip
        if (mainTheoryPath.trim().isEmpty()) {
            ResponseStatusException(HttpStatus.FORBIDDEN, "main theory path must not be empty")
        }
        val userId = getUserId(principal)
        val serviceLevel = getServiceLevel(principal)
        val processingRequest = TheoryCompletionJobRequest(jobResource, mainTheoryPath, serviceLevel, userId!!)
        return processingFacade.prepareProcessing(processingRequest)
    }


    companion object : KLogging()
}