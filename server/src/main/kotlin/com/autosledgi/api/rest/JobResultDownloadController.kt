/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.api.rest

import com.autosledgi.api.security.SecureRepository
import com.autosledgi.exception.ObjectStorageException
import com.autosledgi.jobresult.JobResultObjectRepository
import mu.KLogging
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.io.IOException
import java.security.Principal
import javax.servlet.http.HttpServletResponse


/**
 * Controller for the job result downloads.
 *
 * @author Jens Buerger
 */
@RestController
@RequestMapping("/user/jobresult")
class JobResultDownloadController(
    private val secureRepository: SecureRepository,
    private val jobResultObjectRepository: JobResultObjectRepository
) {

    /**
     * Download the processing results.
     *
     * @param jobId The autosledgi job id that represents the processing job.
     */
    @GetMapping("/{jobId:[\\d]+}")
    fun downloadJobResult(
        @PathVariable jobId: Long,
        response: HttpServletResponse,
        principal: Principal?
    ) {
        try {
            logger.debug { "Resolving job result for jobId $jobId" }
            val job = secureRepository.safelyGetSystemJob(jobId, principal)
            jobResultObjectRepository.getInputStreamForId(job.id!!).use { it.copyTo(response.outputStream) }
            response.flushBuffer()
            response.contentType = "application/zip"
        } catch (e: ObjectStorageException) {
            logger.warn(e) { "ObjectStorageException Exception occurred during file download" }
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Job result not found", e)
        } catch (e: IOException) {
            logger.warn(e) { "IO Exception occurred during job result download" }
            throw e
        }
    }


    companion object : KLogging()
}