/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.api.rest

import com.autosledgi.api.security.getServiceLevel
import mu.KLogging
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal


/**
 * This controller is only a dummy to demonstrate how the REST-API can be created.
 *
 * @author Jens Buerger
 */
@RestController
class DummyController {


    @GetMapping("/user/answer")
    fun answer(principal: Principal?): String {
        for (a in SecurityContextHolder.getContext().authentication.authorities) {
            logger.info { "Authority ${a.authority}" }
        }
        logger.info { "Service Level: ${getServiceLevel(principal)}" }
        return principal?.name ?: "No Principal given"
    }

    @GetMapping("/admin/answer")
    fun adminAnswer(): Int {
        return 21
    }


    companion object : KLogging()
}