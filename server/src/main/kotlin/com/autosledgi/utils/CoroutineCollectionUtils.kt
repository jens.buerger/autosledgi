/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.utils

import kotlinx.coroutines.Job


/**
 * Join multiple coroutines simultaneously.
 *
 * @param jobs The coroutines that should be waited for.
 */
suspend fun awaitAll(jobs: Collection<Job>) = jobs.forEach { it.join() }


/**
 * Cancel multiple jobs in parallel - without waiting for completion.
 *
 * @param jobs The coroutines that should be canceled.
 */
fun cancelAll(jobs: Collection<Job>) = jobs.forEach { it.cancel() }

