/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.persistence.provider.`object`

import org.springframework.core.io.Resource
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.net.URI
import java.nio.file.Path

/**
 * Interface for object providers.
 *
 * @author Jens Buerger
 */
interface GenericObjectStorageProvider {

    val type: ObjectStorageType

    fun getOutputStream(objectURI: URI): OutputStream

    fun uploadFile(file: File, objectURI: URI)

    fun downloadObject(objectURI: URI, targetPath: Path)

    fun getInputStream(objectURI: URI): InputStream

    fun doesObjectExist(objectURI: URI): Boolean

    /*
        * Does not deliver an exception if object does not exist
        */
    fun deleteObject(objectURI: URI)

    fun safeDeleteObject(objectURI: URI)

    fun getResourcesFromSearch(objectURI: URI): Array<Resource>
}