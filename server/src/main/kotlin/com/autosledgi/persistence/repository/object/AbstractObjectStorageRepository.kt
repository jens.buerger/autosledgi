/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.persistence.repository.`object`

import com.autosledgi.common.utils.EMPTY_STRING
import com.autosledgi.exception.ObjectStorageException
import com.autosledgi.persistence.provider.`object`.GenericObjectStorageProvider
import com.autosledgi.persistence.provider.`object`.ObjectStorageRepository
import org.springframework.core.io.Resource
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.net.URI
import java.nio.file.Path

/**
 * Provides a generic repository facade for a provider.
 *
 * It mainly delegates incoming calls, and wraps potential exceptions into an ObjectStorageException.
 *
 * @author Jens Buerger
 */
abstract class AbstractObjectStorageRepository(
    private val provider: GenericObjectStorageProvider,
    private val bucketName: String?,
    private val pathPrefix: String?
) :
    ObjectStorageRepository {

    init {
        if (provider.type.bucketRequired && (bucketName == "null" || bucketName?.trim() == EMPTY_STRING)) {
            throw IllegalArgumentException("object storage provider requires bucket")
        }

        if ((!provider.type.pathPrefixAllowed) && pathPrefix != "") {
            throw IllegalArgumentException("path prefix not allowed for bucket type")
        }
    }

    override fun getOutputStream(path: Path): OutputStream = try {
        provider.getOutputStream(pathToURI(path))
    } catch (e: Exception) {
        throw ObjectStorageException("Output stream broken", e)
    }

    fun getOutputStream(uri: URI): OutputStream = try {
        provider.getOutputStream(uri)
    } catch (e: Exception) {
        throw ObjectStorageException("Output stream broken", e)
    }

    override fun uploadFile(file: File, path: Path) = try {
        provider.uploadFile(file, pathToURI(path))
    } catch (e: Exception) {
        throw ObjectStorageException("Object upload failed", e)
    }

    override fun getInputStream(path: Path): InputStream = try {
        provider.getInputStream(pathToURI(path))
    } catch (e: Exception) {
        throw ObjectStorageException("File URI invalid", e)
    }

    fun getInputStream(uri: URI): InputStream = try {
        provider.getInputStream(uri)
    } catch (e: Exception) {
        throw ObjectStorageException("File URI invalid", e)
    }

    override fun downloadObject(path: Path, targetPath: Path) = try {
        provider.downloadObject(pathToURI(path), targetPath)
    } catch (e: Exception) {
        throw ObjectStorageException("Object download failed", e)
    }

    override fun doesObjectExist(path: Path): Boolean = try {
        provider.doesObjectExist(pathToURI(path))
    } catch (e: Exception) {
        throw ObjectStorageException("Object query failed", e)
    }

    override fun deleteObject(path: Path) = try {
        provider.deleteObject(pathToURI(path))
    } catch (e: Exception) {
        throw ObjectStorageException("Object deletion failed", e)
    }

    override fun safeDeleteObject(path: Path) = try {
        provider.safeDeleteObject(pathToURI(path))
    } catch (e: Exception) {
        throw ObjectStorageException("Object safe delete failed", e)
    }

    override fun getResourcesFromSearch(path: Path): Array<Resource> = try {
        provider.getResourcesFromSearch(pathToURI(path))
    } catch (e: Exception) {
        throw ObjectStorageException("Object query failed", e)
    }

    fun pathToURI(path: Path): URI =
        URI(provider.type.schemaName, bucketName, (pathPrefix ?: "") + path.toString(), null, null)
}

