/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.persistence.jpa

import com.autosledgi.user.KeycloakUserId
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Suppress("unused")
@Converter(autoApply = true)
class KeycloakUserIdConverter : AttributeConverter<KeycloakUserId, String> {

    override fun convertToEntityAttribute(dbData: String?): KeycloakUserId? = dbData?.let { KeycloakUserId(it) }


    override fun convertToDatabaseColumn(attribute: KeycloakUserId?): String? = attribute?.uri

}