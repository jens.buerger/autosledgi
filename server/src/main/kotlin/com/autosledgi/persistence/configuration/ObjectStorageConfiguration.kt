/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.persistence.configuration

import com.autosledgi.cloudprovider.aws.s3.S3Provider
import com.autosledgi.persistence.provider.`object`.GenericObjectStorageProvider
import com.autosledgi.persistence.provider.`object`.ObjectStorageType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ObjectStorageConfiguration(
    @Value("\${autosledgi.jobresource.provider}")
    val jobResourceBundleStorageType: ObjectStorageType,

    @Value("\${autosledgi.jobresult.provider}")
    val jobResultStorageType: ObjectStorageType,

    @Value("\${autosledgi.batchresult.provider}")
    val batchJobResultStorageType: ObjectStorageType,

    @Autowired
    val s3Facade: S3Provider
) {


    @Bean
    @Qualifier("jobResourceBundleProvider")
    fun jobResourceBundleProvider(): GenericObjectStorageProvider =
        defaultProviderResolution(jobResourceBundleStorageType)


    @Bean
    @Qualifier("jobResultProvider")
    fun jobResultProvider(): GenericObjectStorageProvider = defaultProviderResolution(jobResultStorageType)

    @Bean
    @Qualifier("batchJobResultProvider")
    fun batchJobResultProvider(): GenericObjectStorageProvider = defaultProviderResolution(jobResultStorageType)


    private fun defaultProviderResolution(type: ObjectStorageType): GenericObjectStorageProvider {
        return when (type) {
            ObjectStorageType.S3 -> s3Facade
            else -> throw NotImplementedError("Configured Storage provider is not available")
        }
    }
}