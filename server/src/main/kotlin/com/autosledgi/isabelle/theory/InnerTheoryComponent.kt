/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

/**
 * Representation for outer theory components that can be independently processed and updated.
 *
 * Note: Inner components are not independently uneatable!
 *
 * @property theory The theory file that contains the component.
 * @property parent The parent component this inner component
 * @author Jens Buerger
 */
abstract class InnerTheoryComponent : TheoryComponent() {


    override fun updateFile() {
        parent?.updateFile()
            ?: throw IllegalStateException("Component ${this} not backed by theory file")
    }
}

