/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import com.autosledgi.common.isabelle.technical.DEF_INDENT
import com.autosledgi.common.isabelle.technical.isabelleNormalize
import com.autosledgi.common.utils.EMPTY_STRING
import com.autosledgi.common.utils.addPrefix
import com.autosledgi.exception.AutosledgiBusinessException
import mu.KLogging
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


/**
 * Representation of lemmas and theorems within a theory file.
 *
 * @author Jens Buerger
 */
data class Lemma(
    var annotation: LemmaAnnotation?,
    var name: String,
    var options: String?,
    var claim: LemmaClaim?,
    var proof: Proof?
) : OuterTheoryComponent() {


    val unfinished: Boolean
        get() = proof?.unfinished ?: throw AutosledgiBusinessException("Lemma syntax error!")

    /**
     * Parsing constructor
     */
    constructor(rawText: String, theory: Theory?) : this(null, EMPTY_STRING, null, null, null) {
        this.rawText = rawText
        this.theory = theory
        parsePattern?.let {
            val matcher: Matcher = it.matcher(rawText.isabelleNormalize())
            if (matcher.find()) {
                annotation =
                        LemmaAnnotation(matcher.group("annotations") ?: EMPTY_STRING, this)
                name = matcher.group("name") ?: EMPTY_STRING
                options = matcher.group("options") ?: EMPTY_STRING
                claim = LemmaClaim(matcher.group("claim"), this)
                proof = Proof(matcher.group("proof"), this)
                logger.debug { "Parsed Lemma $name" }
            }
        }
    }


    override val region: OuterTheoryComponentRegion
        get() {
            val pattern = Pattern.compile(
                """(?:\(\*OPTIONS:(.*?)\*\).*?|)"""
                        + """(?:lemma|theorem)(?:\s+$name\s*(\[.*?\]\s*?|):)(.*?)(?=proof|apply|by|sorry)(.*?)(?=end|lemma|theorem|\Z)""",
                Pattern.DOTALL
            )
            return OuterTheoryComponentRegion(pattern, theory)
        }

    override val text: List<String>
        get() {
            val text: MutableList<String> = ArrayList()
            annotation?.let { text.addAll(it.text) }
            // TODO make the following line more beautiful and add condition for the colon
            val optionText: String? = options?.let { if (it.trim() != "") " " + it.trim() else "" }
            text += "lemma $name ${optionText ?: EMPTY_STRING} ${if (name != "") ":" else ""}".trim()
            text += claim?.text?.addPrefix(DEF_INDENT) ?: throw java.lang.IllegalStateException("No claim present for Lemma $name")
            text += proof?.text?.addPrefix(DEF_INDENT) ?: throw java.lang.IllegalStateException("No proof present for Lemma $name")
            return text
        }


    val anonymous: Boolean
        get() = name.trim() == EMPTY_STRING

    companion object : KLogging()


}