/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import com.autosledgi.common.isabelle.technical.isabelleNormalize
import com.autosledgi.common.utils.splitLines

/**
 * Represents the complete annotations of a lemma.
 *
 * @author Jens Buerger
 */
data class LemmaAnnotation(val annotations: MutableList<String>) : InnerTheoryComponent() {

    /**
     * Parsing constructor.
     */
    constructor(rawText: String, parent: Lemma?) : this(ArrayList(splitLines(rawText).isabelleNormalize())) {
        this.rawText = rawText
        this.parent = parent
    }


    override val text: List<String>
        get() = annotations


    val empty: Boolean
        get() = annotations.isEmpty()
}