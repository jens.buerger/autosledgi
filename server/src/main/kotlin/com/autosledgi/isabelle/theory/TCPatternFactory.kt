/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import com.autosledgi.common.utils.outerPattern
import java.util.regex.Pattern
import kotlin.reflect.KClass

/**
 * Get the regex pattern for outer theory file components
 */
object TCPatternFactory {

    // Lemma Regex
    private fun comment(content: String) = """\(\*\s*$content\s*\*\)"""

    private fun annotations(named: Boolean = true) = comment("OPTIONS:(${if (named) "?<annotations>" else ""}.*?)")
    private val annotations = annotations()
    private fun name(n: String? = null) = """(?<name>${n ?: """\S+?"""})"""
    private const val options = """\[(?<options>.*?)\]"""
    private fun header(lemmaName: String? = null) =
        """(?:$annotations)?\s*?(?:lemma|theorem)\s*?(?:\s${name(lemmaName)})?\s*(?:$options)?(?::)?"""

    private val header = header()
    private const val innerLanguage = """(?:"(.*?)")"""
    private const val optionalAssumptionName = """(?:(?:\s+?\w+?:)|)"""
    private const val assumeProof =
        """(assumes$optionalAssumptionName\s*$innerLanguage\s+(and$optionalAssumptionName\s*$innerLanguage)*)?\s*shows$optionalAssumptionName\s*$innerLanguage"""
    private const val claim = """(?<claim>$innerLanguage|$assumeProof)"""
    private val proof = """(?<proof>.*?)\s*(?=end|lemma|theorem|${annotations(false)}|\Z)"""

    // Pattern per class
    val pattern: Map<KClass<*>, Pattern> = mapOf(
        Theory::class to Pattern.compile(""".*theory(.*?)imports(.*?)begin(.*)end""", Pattern.DOTALL),
        TheoryImport::class to Pattern.compile("(\\S+)"),
        LemmaAnnotation::class to Pattern.compile("""(?:\(\*OPTIONS>(.*?)\*\).*?|)"""),
        Lemma::class to Pattern.compile("""$header\s*$claim\s*$proof""", Pattern.DOTALL),
        ProofStatement::class to Pattern.compile(
            """(proof.*?qed(?=[\s\n]*?(?:apply|sorry|oops|\Z))|(?:apply|by)(?:\s|\n)*\(.*?\)|sorry|oops)""",
            Pattern.DOTALL
        )

    )

    // Pattern per class, all groups removed and wrapped into one large outer group
    val outerPattern: Map<KClass<*>, Pattern> = pattern.mapValues { (_, value) -> value.outerPattern() }
}