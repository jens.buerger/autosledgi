/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import com.autosledgi.common.isabelle.technical.isabelleNormalize
import com.autosledgi.common.utils.concatLines
import mu.KLogging
import java.util.regex.Matcher
import java.util.regex.Pattern

data class TheoryImports(
    var theories: MutableSet<TheoryImport>
) : OuterTheoryComponent() {

    private var fileUpdatedOnce: Boolean = true


    /**
     * Parsing constructor.
     */
    constructor(rawText: String, theory: Theory?) : this(HashSet<TheoryImport>()) {
        fileUpdatedOnce = false
        this.rawText = rawText
        this.theory = theory
        theories = HashSet()
        val matcher: Matcher =
            TCPatternFactory.outerPattern.getValue(TheoryImport::class).matcher(rawText.isabelleNormalize())
        while (matcher.find()) theories.add(TheoryImport(matcher.group(1)))
    }

    constructor(theory: Theory?) : this(HashSet<TheoryImport>()) {
        this.theory = theory
    }

    constructor() : this(null)

    /**
     *
     * @throws IllegalStateException iff the region cannot be determined
     */
    override val region: OuterTheoryComponentRegion
        get() {
            val pattern: Pattern = when {
                fileUpdatedOnce -> Pattern.compile(text.concatLines())
                rawText == null -> throw IllegalStateException("cannot retrieve region")
                else -> rawText.let {
                    return OuterTheoryComponentRegion(
                        Pattern.compile(Pattern.quote(it)),
                        theory
                    )
                }
            }

            return OuterTheoryComponentRegion(pattern, theory)
        }

    override val text: List<String>
        get() = listOf(theories.joinToString(" "))


    override fun updateFile() {
        fileUpdatedOnce = true
        super.updateFile()
    }

    companion object : KLogging()
}