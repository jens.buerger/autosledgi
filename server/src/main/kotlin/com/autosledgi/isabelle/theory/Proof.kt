/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import com.autosledgi.common.isabelle.technical.ProofMechanism
import com.autosledgi.common.isabelle.technical.SledgehammerOptions
import com.autosledgi.common.isabelle.technical.isabelleNormalize

/**
 * Representation of Lemma and Theorem Proofs
 *
 * @author Jens Buerger
 */
data class Proof(
    var statements: MutableList<ProofStatement>
) : InnerTheoryComponent() {


    /**
     * Parsing constructor
     */
    constructor(rawText: String, parent: Lemma?) : this(ArrayList()) {
        this.rawText = rawText
        this.parent = parent
        val normalizedText = rawText.isabelleNormalize()
        val matcher = TCPatternFactory.pattern.getValue(ProofStatement::class).matcher(normalizedText)
        while (matcher.find()) statements.add(
            ProofStatement(
                matcher.group(1),
                this
            )
        )
    }


    override val text: List<String>
        get() = statements.flatMap { it.text }


    val unfinished: Boolean
        get() = statements.any { it.unfinished }


    val sorries: Int
        get() = statements.fold(0) { total, statement -> total + statement.sorries }


    val proofMechanisms: Set<ProofMechanism>
        get() = statements.flatMap { it.proofMechanisms }.toSet()

    /**
     * Internal persistence class for sorry contexts.
     *
     * @property statement The proof statement that has the sorry.
     * @property sorryIndex The sorry index within [statement]
     */
    private data class SorryContext(val statement: ProofStatement, val sorryIndex: Int)


    private fun proofStatementForSorryIndex(sorryIndex: Int): SorryContext {
        if (sorryIndex !in 0 until sorries) throw IndexOutOfBoundsException("Sorry index out-of-bounds")
        var curIndex = 0
        for (statement in statements) {
            if (sorryIndex < sorries + curIndex) {
                return SorryContext(statement, sorryIndex - curIndex)
            } else {
                curIndex += sorries
            }
        }
        throw IndexOutOfBoundsException("Sorry index out-of-bounds")
    }


    fun openSubgoals(sorryIndex: Int) = proofStatementForSorryIndex(sorryIndex).statement.openSubgoals


    fun injectSledgehammerCommand(sorryIndex: Int, options: SledgehammerOptions) =
        proofStatementForSorryIndex(sorryIndex).let { it.statement.injectSledgehammerCommand(it.sorryIndex, options) }

    fun injectProofStatement(sorryIndex: Int, proofStatement: ProofStatement) =
        proofStatementForSorryIndex(sorryIndex).let { it.statement.injectProofStatement(it.sorryIndex, proofStatement) }

    fun injectString(sorryIndex: Int, replacement: String) =
        proofStatementForSorryIndex(sorryIndex).let { it.statement.replaceSorry(it.sorryIndex, replacement) }

}