/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import com.autosledgi.common.utils.outerPattern
import java.util.regex.Pattern

/**
 * Representation of outer theory components that can be independently processed and updated.
 *
 * @property rawText The raw text that can be used to parse the component.
 * @property theory The theory file that contains the component.
 * @author Jens Buerger
 */
abstract class OuterTheoryComponent : TheoryComponent() {

    val parsePattern: Pattern?
        get() = TCPatternFactory.pattern[this::class]

    /**
     * The pattern that is used to identify and replace the component in a theory file.
     */
    abstract val region: OuterTheoryComponentRegion

    val outerPattern: Pattern
        get() = region.pattern.outerPattern()

    override fun updateFile() {
        theory?.updateOuterTheoryComponentRegion(region, text)
            ?: throw IllegalStateException("Component ${this} not backed by theory file")
    }
}