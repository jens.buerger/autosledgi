/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import com.autosledgi.common.isabelle.technical.ProofMechanism
import com.autosledgi.common.isabelle.technical.SledgehammerOptions
import com.autosledgi.common.isabelle.technical.isabelleNormalize
import com.autosledgi.common.utils.concatLines
import com.autosledgi.common.utils.numOfMatches
import com.autosledgi.common.utils.splitLines
import java.time.Duration

/**
 * Represents proof statements.
 *
 * @author Jens Buerger
 */
data class ProofStatement(
    var proofStatements: MutableList<String>,
    val duration: Duration?
) : InnerTheoryComponent() {


    /**
     * Main parsing constructor
     */
    constructor(rawText: String, parent: Proof?, duration: Duration?) : this(ArrayList(), duration) {
        this.rawText = rawText
        this.parent = parent
        proofStatements = splitLines(rawText.trimIndent()).isabelleNormalize().toMutableList()
    }

    /**
     * Parsing constructor.
     */
    constructor(rawText: String, parent: Proof?) : this(ArrayList(), null) {
        this.rawText = rawText
        this.parent = parent
        proofStatements = splitLines(rawText.trimIndent()).isabelleNormalize().toMutableList()
    }

    val unfinished: Boolean
        get() = ProofMechanism.SORRY.pattern.matcher(proofStatements.concatLines()).find()

    val sorries: Int
        get() = proofStatements.concatLines().numOfMatches(ProofMechanism.SORRY.pattern)

    val oops: Boolean
        get() = ProofMechanism.OOPS.pattern.matcher(proofStatements.concatLines()).find()

    val openSubgoals: Int
        get() = TODO("Currently not implemented")

    val proofMechanisms: Set<ProofMechanism>
        get() = ProofMechanism.parseProofMechanism(proofStatements.concatLines())

    override val text: List<String>
        get() = proofStatements


    fun injectSledgehammerCommand(sorryIndex: Int, options: SledgehammerOptions) =
        replaceSorry(sorryIndex, surroundByNewLine(options.command))


    fun injectProofStatement(sorryIndex: Int, proofStatement: ProofStatement) =
        replaceSorry(sorryIndex, surroundByNewLine(proofStatement.proofStatements.concatLines()))


    fun replaceSorry(sorryIndex: Int, replacement: String) {
        val sorryMatcher = ProofMechanism.SORRY.pattern.matcher(proofStatements.concatLines())
        var index = -1
        while (sorryMatcher.find()) {
            index++
            if (index == sorryIndex) {
                val newText =
                    proofStatements.concatLines().replaceRange(sorryMatcher.start(), sorryMatcher.end(), replacement)
                proofStatements = ArrayList(splitLines(newText))
            }
        }
    }

    private fun surroundByNewLine(string: String) = "\n$string\n\n"


}