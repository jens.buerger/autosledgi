/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import com.autosledgi.job.system.SystemJob
import com.autosledgi.job.system.SystemJobRepository
import com.autosledgi.job.system.jobwrapper.TheoryCompletionJob
import com.autosledgi.job.system.jobwrapper.TheoryCompletionJobRequest
import org.springframework.stereotype.Component
import java.io.File

/**
 * Facade for isabelle theory processing/completion.
 *
 * @author Jens Buerger
 */
@Component
class TheoryProcessingFacade(
    private val jobRepository: SystemJobRepository
) {


    /**
     * Create the required jobs
     *
     * @param request The job processing request.
     */
    fun prepareProcessing(request: TheoryCompletionJobRequest): SystemJob {
        val theoryCompletionJob = TheoryCompletionJob(request)
        jobRepository.save(theoryCompletionJob)
        return theoryCompletionJob
    }


    /**
     * Get processed theory file
     *
     * @param jobId The autosledgi job id that represents the processing job.
     */
    fun getProcessedTheoryFile(jobId: Long): File {
        TODO(
            """Search for job
            |If job is not finished throw exception (AppException)
            |Download file from object storage and pass it on
        """.trimMargin()
        )
    }


}