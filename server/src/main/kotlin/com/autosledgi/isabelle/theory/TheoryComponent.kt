/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

/**
 * Class for theory file components.
 *
 * @property theory The theory file that contains the component.
 * @property rawText The text that represents the component in the theory file.
 * @property parent The parent component the component.
 * @author Jens Buerger
 */
abstract class TheoryComponent {

    var subComponents: MutableList<TheoryComponent> = ArrayList()
    open var parent: TheoryComponent? = null
    var theory: Theory? = null
    protected var rawText: String? = null

    abstract val text: List<String>


    /**
     * Update the theory file based on the component contents.
     *
     */
    abstract fun updateFile()
}