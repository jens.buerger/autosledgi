/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import com.autosledgi.common.isabelle.technical.isabelleNormalize
import com.autosledgi.common.textfile.TextFile
import com.autosledgi.common.utils.EMPTY_STRING
import com.autosledgi.common.utils.concatLines
import com.autosledgi.common.utils.findNthMatch
import com.autosledgi.common.utils.renameFile
import com.autosledgi.common.versioned.textfile.VerTextFile
import mu.KLogging
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Path
import java.util.regex.Matcher
import java.util.regex.Pattern
import javax.validation.constraints.NotEmpty


/**
 * Representation of Isabelle theory files
 *
 * @property name The theory name.
 * @property theoryImports Internal imports representation.
 * @property _lemmas Internal lemma representation.
 * @property textFile The backing text file.
 * @author Jens Buerger
 */
class Theory private constructor(
    private var theoryImports: TheoryImports,
    private var _lemmas: MutableList<Lemma>,
    private val textFile: TextFile
) : VerTextFile(textFile) {

    var name: String = ""
        set(@NotEmpty value) {
            // First call is constructor, do not alter content or move the file
            if (field != "") {
                @Suppress("RegExpRedundantEscape")
                textFile.replaceFirstMatch(
                    Pattern.compile("""^(\s*)(theory)(\s+)([A-Za-z0-9$\_]+)"""),
                    """$1$2$3$value"""
                )

                textFile.path?.let { textFile.moveToFile(it.renameFile("$value.thy")) }
            }
            field = value
        }

    val lemmas: List<Lemma>
        get() = _lemmas.toList()


    /** Add lemma at specified position. If no position was specified, adds to the end of the list. */
    fun addLemma(lemma: Lemma, position: Int = _lemmas.size) {
        assert(0 <= position && position <= _lemmas.size)

        val cursor = if (position < lemmas.size) {
            // Set the cursor right before the next lemma
            textFile.contents.findNthMatch(lemma.outerPattern, position + 1).start
        } else {
            // Set the cursor right after the last lemma
            textFile.contents.findNthMatch(lemma.outerPattern, position).end
        }

        _lemmas.add(position, lemma)

        // Split the line right at the cursor
        val lineToSplit = textFile.contents[cursor.lineNr]
        textFile.setLine(cursor.lineNr, lineToSplit.substring(0, cursor.colNr))
        textFile.insertLine(cursor.lineNr + 1, lineToSplit.substring(cursor.colNr))

        // Insert the lemma between the newly split lines
        textFile.insertLine(cursor.lineNr + 1, "")
        textFile.insertLines(cursor.lineNr + 1, lemma.text)
        if (cursor.colNr != 0) textFile.insertLine(cursor.lineNr + 1, "") // splitting might already yield a line
    }

    /** Remove the specified lemma */
    fun removeLemma(lemma: Lemma): Boolean {
        val index = _lemmas.indexOf(lemma)
        if (_lemmas.remove(lemma)) {
            // One-based, therefore index+1
            contents = textFile.replaceNthMatch(lemma.outerPattern, index + 1, "").toMutableList()
            return true
        }
        return false
    }

    // TODO the setter of this will be interesting (update the file if necessary but lazy)

    // facade property for imports
    var imports: Set<TheoryImport>
        get() = theoryImports.theories
        set(value) {
            theoryImports.theories = value.toMutableSet()
        }


    private fun parseLemma(text: String): MutableList<Lemma> {
        val res = ArrayList<Lemma>()
        val matcher = TCPatternFactory.outerPattern.getValue(Lemma::class).matcher(text)
        while (matcher.find()) {
            res += Lemma(matcher.group(1), this)
        }
        return res
    }

    constructor(textFile: TextFile) : this(
        TheoryImports(), emptyList<Lemma>().toMutableList(), textFile
    ) {
        logger.debug { "Trying to parse ${textFile.path} as theory file" }
        val matcher: Matcher = COARSE_THEORY_FILE_PATTERN.matcher(textFile.contents.concatLines())
        if (matcher.find()) {
            name = matcher.group(1).isabelleNormalize().trim()
            theoryImports = TheoryImports(matcher.group(2), this)
            _lemmas = parseLemma(matcher.group(3))
        } else {
            logger.debug { "Parsing of ${textFile.path} as theory file failed" }
            throw IllegalArgumentException("${textFile.path} could not be parsed as a theory")
        }
    }

    constructor(inputStream: InputStream) : this(TextFile(inputStream))


    constructor(path: Path) : this(TextFile(path))


    constructor(name: String, theoryImports: TheoryImports, lemmas: List<Lemma>) : this(
        theoryImports,
        lemmas.toMutableList(),
        TextFile()
    ) {
        theoryImports.theory = this
        this.name = name
        TODO("Also update lemma references, In this case the theory file content should be generated automatically")
    }


    /**
     * Update a outer component region in a theory file with new text.
     *
     * @param region The component region.
     * @param newLines The replacement lines.
     */
    fun updateOuterTheoryComponentRegion(region: OuterTheoryComponentRegion, newLines: List<String>) =
        textFile.replaceFirstMatch(region.pattern, newLines.concatLines())


    /**
     * Updates all the lemmas
     */
    fun updateAllLemmasInTextFile() = _lemmas.forEach { it.updateFile() }


    /**
     * Get the outputStream for the contents
     */
    fun copyContentsToOutputStream(outputStream: OutputStream) = textFile.copyToOutputStream(outputStream)

    fun getInputStream(): InputStream = textFile.getInputStream()

    /**
     * Just reparses the saved content input
     *
     * @note Make sure to call updateAllLemmasInTextFile if you want the most current version of the copy
     */
    fun getDeepCopy(): Theory {
        // TODO OPT this is just a very basic implementation for the copy function
        return Theory(this.textFile.getDeepCopy())
    }

    /**
     * Basic function to get lemma sibling in copy
     */
    fun getLemmaSiblingInCopy(copy: Theory, lemma: Lemma): Lemma? {
        val resultsByName = copy.lemmas.filter { it.name == lemma.name }
        if (resultsByName.isNotEmpty() && lemma.name.trim() != EMPTY_STRING) {
            if (resultsByName.size > 1) {
                logger.warn { "Identified more than one sibling of lemma with name ${lemma.name}" }
            }
            return resultsByName.first()
        }
        val resultsByRegion = copy.lemmas.filter { it.region == lemma.region }
        if (resultsByRegion.isNotEmpty()) {
            logger.info { "Identified lemma sibling by region!" }
            return resultsByRegion.first()
        }
        return null
    }

    companion object : KLogging() {
        val COARSE_THEORY_FILE_PATTERN: Pattern =
            Pattern.compile(""".*theory(.*?)imports(.*?)begin(.*)end""", Pattern.DOTALL)

    }
}