/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.output

/**
 * Enum class for isabelleServerOutput message types
 *
 * @author Pascal Bontempi
 */
enum class MessageType(val messageIdentifier: String) {
    NOTE("NOTE"),
    FINISHED("FINISHED"),
    FAILED("FAILED"),
    OK("OK");

    companion object {
        fun getStringValues(): MutableList<String> {
            val list: MutableList<String> = arrayListOf()
            for (type in MessageType.values()){
                list.add(type.messageIdentifier)
            }
            return list
        }
    }
}