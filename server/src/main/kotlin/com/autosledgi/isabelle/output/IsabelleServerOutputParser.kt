/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.output

import com.autosledgi.common.utils.EMPTY_STRING
import com.autosledgi.isabelle.theory.ProofStatement
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.joda.time.format.PeriodFormatterBuilder
import org.springframework.stereotype.Component
import java.io.File
import java.io.InputStream
import java.nio.file.Path
import java.time.Duration

@Suppress("RegExpRedundantEscape")
private const val DURATION_REGEX = """\(([0-9\.]*?\sms)\)"""
private val DURATION_FORMATTER = PeriodFormatterBuilder()
    .appendMillis().appendSuffix(" ms")
    .appendSeconds().appendSuffix(" s")
    .appendSeconds().appendSeparator(",").appendDays().appendSuffix(" s")
    .toFormatter()

/**
 * Parser for the response outputs
 *
 * @author Pascal Bontempi
 */
@Component
class IsabelleServerOutputParser : IOutputParser<IsabelleServer2018Response> {

    /**
     * @param path path to the outputfile
     * @return Returns an object with ok-, failed-, note- and finished-responses
     */
    override fun loadAndParseFile(path: Path): List<IsabelleServer2018Response> =
        File(path.toString()).inputStream().use { parseStream(it) }


    override fun parseStream(inputStream: InputStream): List<IsabelleServer2018Response> {
        // not every node of the json-string has to be known
        val mapper = jacksonObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        val testResponseIsabelle: MutableList<IsabelleServer2018Response> = ArrayList()

        // iterates line by line over the file and transforms json-strings into kotlin objects
        inputStream.bufferedReader().useLines { lines ->
            lines.forEach {
                if (MessageType.getStringValues().contains(it.substringBefore(" "))) {
                    val note: IsabelleServer2018Response = mapper.readValue(reformatResponseString(it))
                    testResponseIsabelle.add(note)
                }
            }
        }
        return testResponseIsabelle
    }


    fun getProofStatementsFromSledgehammerOutput(isabelleResponse: List<IsabelleServer2018Response>): Set<ProofStatement> {
        val lastServerResponse = (isabelleResponse.last() as IsabelleServer2018FinishedResponse)
        val lastMessage = lastServerResponse.nodes?.first()?.messages?.map { it.message }!!
        val allSledgehammerOutputs = lastMessage.filter { "Try this".toRegex().containsMatchIn(it) }
            .map { it.split(Regex("""(?<=Try this:).*?""")).component2() }
        val durations =
            allSledgehammerOutputs.map {
                try {
                    Regex(DURATION_REGEX).find(it)?.groupValues?.get(1)
                } catch (e: Exception) {
                    EMPTY_STRING
                }
            }
        val cleanedOutput =
            allSledgehammerOutputs.map { Regex("""\(No Isar proof available\.\)""").replace(it, EMPTY_STRING).trim() }
        val proofStrings = cleanedOutput.map { Regex(DURATION_REGEX).replace(it, EMPTY_STRING) }
        return (proofStrings zip durations).map { (proofStr, duration) ->
            ProofStatement(
                proofStr,
                null,
                getDurationFromIsabelleStr(duration)
            )
        }.toSet()

    }

    // returns 1 second if parsing failed
    private fun getDurationFromIsabelleStr(durationString: String?): Duration? {
        if (durationString == null) {
            return null
        }

        // TODO OPT hacky implementation
        var duration: Duration? = try {
            Duration.ofMillis(DURATION_FORMATTER.parsePeriod(durationString).toStandardDuration().millis)
        } catch (e: Exception) {
            if (durationString.contains('s')) {
                Duration.ofSeconds(1)
            } else {
                null // probably timeout
            }
        }
        if (duration?.toDays() ?: 0 > 0) {
            // postprocess ms
            val days = duration?.toDays()!!
            duration = duration.minusDays(days)
            duration = duration.plusMillis((("0.$days").toDouble() * 1000).toLong())
        }
        return duration
    }
}
