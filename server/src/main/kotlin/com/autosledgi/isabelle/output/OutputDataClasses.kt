/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.output

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo

/**
 * All these data classes exist for parsing the output files
 *
 * @author Pascal Bontempi
 */

// different types of responses
data class IsabelleServer2018NoteResponse(
    @JsonProperty("kind") val kind: String,
    @JsonProperty("message") val message: String?,
    @JsonProperty("theory") val theory: String?,
    @JsonProperty("session") val session: String?,
    override var task: String?) : IsabelleServer2018Response(task)

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
data class IsabelleServer2018FinishedResponse(
    @JsonProperty("ok") val ok: Boolean,
    @JsonProperty("return_code") val return_code: String?,
    @JsonProperty("tmp_dir") val tmp_dir: String?,
    @JsonProperty("isabelleServer2018Sessions") val isabelleServer2018Sessions: MutableList<IsabelleServer2018Session>?,
    @JsonProperty("session_id") val session_id: String?,
    @JsonProperty("errors") val isabelleServer2018Errors: MutableList<IsabelleServer2018Message>?,
    @JsonProperty("nodes") val nodes: MutableList<IsabelleServer2018Node>?,
    override var task: String?
) : IsabelleServer2018Response(task)

data class IsabelleServer2018OkResponse(
    @JsonProperty("isabelle_id") val isabelle_id: String?,
    @JsonProperty("isabelle_version") val isabelle_version: String?,
    override var task: String?) : IsabelleServer2018Response(task)

data class IsabelleServer2018FailedResponse(
    @JsonProperty("kind") val kind: String,
    @JsonProperty("message") val message: String?,
    override var task: String?) : IsabelleServer2018Response(task)

// data classes that are used by responses
data class IsabelleServer2018Session(
    @JsonProperty("isabelleServer2018Timing")val isabelleServer2018Timing: IsabelleServer2018Timing?,
    val return_code: String?,
    val session: String?,
    val ok: Boolean?,
    val timeout: Boolean?)

data class IsabelleServer2018Timing(
    val elapsed: Float,
    val cpu: Float,
    val gc: Float)

data class IsabelleServer2018Node(
    val messages: MutableList<IsabelleServer2018Message>?,
    val theory_name: String,
    val node_name: String,
    val status: IsabelleServer2018Status?)

data class IsabelleServer2018Message(
    val kind: String,
    val message: String,
    val pos: IsabelleServer2018Pos?)

data class IsabelleServer2018Status(
    val unprocessed: Int,
    val initialized: Boolean,
    val running: Int,
    val finished: Int,
    val failed: Int,
    val total: Int,
    val consolidated: Boolean,
    val ok: Boolean,
    val warned: Int)

data class IsabelleServer2018Pos(
    val line: Int,
    val offset: Int,
    val end_offset: Int,
    val file: String)

/**
 * This class exists as superclass of the four response classes
 * It is also important that the order of the messages is adhered to. So to store all messages in an MutableList
 * we probably need this superclass
 */
@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type"
)
@JsonSubTypes(
    JsonSubTypes.Type(value = IsabelleServer2018OkResponse::class, name = "OK"),
    JsonSubTypes.Type(value = IsabelleServer2018FailedResponse::class, name = "FAILED"),
    JsonSubTypes.Type(value = IsabelleServer2018NoteResponse::class, name = "NOTE"),
    JsonSubTypes.Type(value = IsabelleServer2018FinishedResponse::class, name = "FINISHED")
)
abstract class IsabelleServer2018Response(@JsonProperty("task") open var task: String?)