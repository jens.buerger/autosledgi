/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.cloudprovider.aws.s3

import com.amazonaws.services.s3.AmazonS3
import com.autosledgi.persistence.provider.`object`.GenericObjectStorageProvider
import com.autosledgi.persistence.provider.`object`.ObjectStorageType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.aws.core.io.s3.PathMatchingSimpleStorageResourcePatternResolver
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Primary
import org.springframework.core.io.Resource
import org.springframework.core.io.ResourceLoader
import org.springframework.core.io.WritableResource
import org.springframework.core.io.support.ResourcePatternResolver
import org.springframework.stereotype.Component
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.net.URI
import java.nio.file.Files
import java.nio.file.Path

/**
 * Facade of the AWS S3 client.
 *
 * @author Jens Buerger
 */
@Primary
@Component
class S3Provider(
    private val resourceLoader: ResourceLoader,
    private val amazonS3: AmazonS3
) : GenericObjectStorageProvider {

    override val type: ObjectStorageType = ObjectStorageType.S3

    private var resourcePatternResolver: ResourcePatternResolver? = null

    @Autowired
    fun setupResolver(applicationContext: ApplicationContext, amazonS3: AmazonS3) {
        this.resourcePatternResolver = PathMatchingSimpleStorageResourcePatternResolver(amazonS3, applicationContext)
    }

    override fun getOutputStream(objectURI: URI): OutputStream =
        (resourceLoader.getResource(objectURI.toString()) as WritableResource).outputStream


    override fun uploadFile(file: File, objectURI: URI): Unit =
        getOutputStream(objectURI).use { outputStream -> Files.copy(file.toPath(), outputStream) }


    override fun getInputStream(objectURI: URI): InputStream =
        resourceLoader.getResource(objectURI.toString()).inputStream


    override fun downloadObject(objectURI: URI, targetPath: Path) {
        Files.copy(getInputStream(objectURI), targetPath)
    }


    override fun doesObjectExist(objectURI: URI) = amazonS3.doesObjectExist(objectURI.host, objectURI.path.objPath())

    /*
     * Does not deliver an exception if object does not exist
     */
    override fun deleteObject(objectURI: URI) = amazonS3.deleteObject(objectURI.host, objectURI.path.objPath())


    override fun safeDeleteObject(objectURI: URI) {
        if (!doesObjectExist(objectURI)) throw IllegalArgumentException("S3 object $objectURI does not exist")
        deleteObject(objectURI)
    }


    override fun getResourcesFromSearch(objectURI: URI): Array<Resource> =
        resourcePatternResolver?.getResources(objectURI.toString())
            ?: throw IllegalStateException("Resource resolver not ready")


    // object paths don't have a leading slash
    private fun String.objPath() = replaceFirst("/", "")
}