/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.cloudprovider.aws.batch

import com.amazonaws.services.batch.AWSBatch
import com.amazonaws.services.batch.model.*
import com.autosledgi.exception.BatchSystemException
import com.autosledgi.job.batch.BatchJob
import com.autosledgi.job.batch.BatchProvider
import com.autosledgi.job.system.JobPriority
import com.autosledgi.job.system.JobStatus
import mu.KLogging
import org.springframework.stereotype.Component
import java.math.BigDecimal
import java.time.Duration

const val BATCH_JOB_ID_ENV_VAR = "AUTOSLEDGI_JOB_ID"

/**
 *
 * Facade of the Amazon Batch Service to prevent unintended side effects.
 *
 * @author Jens Buerger
 */
@Component
class AWSBatchProvider(private val batch: AWSBatch) : BatchProvider {

    // memory is counted in bytes
    override suspend fun submitAndRun(batchJob: BatchJob): BatchJob {
        addJobIdToEnvironment(batchJob)
        val containerOverrides = getContainerOverrides(batchJob)
        val request = getJobRequest(batchJob, containerOverrides)

        logger.info { "submitting AWS batch job" }
        val result = batch.submitJob(request)
        batchJob.status = JobStatus.SUBMITTED
        batchJob.externalId = result.jobId
        logger.info { "FINISHED: submission of batch job with id ${batchJob.externalId}" }
        return batchJob
    }

    private fun addJobIdToEnvironment(batchJob: BatchJob) =
        batchJob.id?.let { batchJob.environment[BATCH_JOB_ID_ENV_VAR] = it.toString() }


    private fun getJobRequest(batchJob: BatchJob, containerOverrides: ContainerOverrides): SubmitJobRequest =
        SubmitJobRequest().also {
            it.jobName = getJobName(batchJob)
            it.jobQueue = getAWSQueueForJob(batchJob)
            it.jobDefinition = getJobDefinitionForJob(batchJob)
            it.containerOverrides = containerOverrides
            it.timeout = durationToTimeout(batchJob.timeout)
        }


    private fun getContainerOverrides(batchJob: BatchJob) =
        ContainerOverrides().also { override ->
            if (batchJob.environment.isNotEmpty()) {
                override.setEnvironment(batchJob.environment.map { (key, value) ->
                    KeyValuePair().withName(key).withValue(value)
                })
            }
            if (batchJob.command.isNotEmpty()) override.setCommand(batchJob.command.asList())
            batchJob.cpuCores?.let { override.vcpus = it }
            batchJob.memory?.let { override.memory = it }
        }


    override suspend fun cancel(batchJob: BatchJob) {
        when {
            batchJob.externalId == null -> throw IllegalArgumentException("External Batch ID must not be null")
            else -> {
                val request = CancelJobRequest().also {
                    it.jobId = batchJob.externalId
                    it.reason = "Autosledgi"
                }
                batch.cancelJob(request)
            }
        }
    }


    override suspend fun terminate(batchJob: BatchJob) {
        when {
            batchJob.externalId == null -> throw IllegalArgumentException("External Batch ID must not be null")
            else -> {
                val request = TerminateJobRequest().also {
                    it.jobId = batchJob.externalId
                    it.reason = "Autosledgi"
                }
                batch.terminateJob(request)
            }
        }
    }


    override suspend fun getStatus(batchJob: BatchJob): JobStatus =
        batchJob.externalId?.let { JobStatus.valueOf(getJobDetailsById(batchJob).status) } ?: JobStatus.UNSPECIFIED

    override suspend fun isFinished(batchJob: BatchJob): Boolean =
        getStatus(batchJob) in listOf(JobStatus.FAILED, JobStatus.SUCCEEDED)

    override suspend fun getRuntime(batchJob: BatchJob): Duration? {
        val jobDetails = batchJob.externalId?.let { getJobDetailsById(batchJob) }
            ?: throw BatchSystemException("Cannot retrieve job state")
        val startTime = jobDetails.startedAt
        val endTime = jobDetails.stoppedAt
        return if (startTime == null || endTime == null) {
            null
        } else {
            Duration.ofMillis(endTime - startTime)
        }
    }

    override suspend fun getCost(batchJob: BatchJob): BigDecimal? {
        // TODO implement this in a meaningful way
        return BigDecimal.ONE
    }


    // the following a rather bookmarks and function for evaluation
    private suspend fun getJobDetailsById(batchJob: BatchJob): JobDetail {
        checkIfSubmitted(batchJob)
        val request = DescribeJobsRequest().withJobs(batchJob.externalId)
        val jobs: MutableList<JobDetail> = batch.describeJobs(request).jobs
        return if (jobs.isEmpty()) {
            throw BatchSystemException("Details for Job with external id ${batchJob.externalId} could not be retrieved")
        } else {
            jobs.first()
        }
    }


    // TODO implement this in a smart way via application properties
    private fun getAWSQueueForJob(batchJob: BatchJob): String =
        when (batchJob.priority) {
            JobPriority.PRICE_DRIVEN -> "prio-0"
            else -> "prio0-instant-test"
        }

    // TODO implement this in a smart way via application properties
    private fun getJobDefinitionForJob(batchJob: BatchJob): String = batchJob.getJobDefinition()


    private fun getJobName(batchJob: BatchJob): String = "ASL-${batchJob.id}-type-${batchJob.javaClass.simpleName}"


    private fun durationToTimeout(duration: Duration): JobTimeout =
        JobTimeout().withAttemptDurationSeconds(duration.seconds.toInt())


    private fun checkIfSubmitted(batchJob: BatchJob) {
        if (batchJob.externalId.isNullOrBlank()) throw java.lang.IllegalArgumentException("Batch Job must be submitted")
    }

    companion object : KLogging()


}