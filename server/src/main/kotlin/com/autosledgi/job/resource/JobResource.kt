/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.resource


import com.autosledgi.job.system.JOB_RESOURCE_ATTRIBUTE
import com.autosledgi.job.system.SystemJob
import com.autosledgi.user.KeycloakUserId
import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.domain.Persistable
import org.springframework.data.util.ProxyUtils
import java.net.URI
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull


const val JOB_RESOURCE_TABLE = "job_resource"

//table columns
private const val JOB_RESOURCE_SEQUENCE = "autosledgi_job_resource_sequence"
private const val ID_COLUMN = "id"
private const val USER_ID_COLUMN = "user_id"
private const val CREATED_ON_COLUMN = "created_on"
private const val URI_COLUMN = "uri"


/**
 * Resource Descriptor for autosledgi job resources
 *
 * @author Jens Buerger
 */
@Entity
@Table(name = JOB_RESOURCE_TABLE)
class JobResource(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "job_resource_generator")
    @SequenceGenerator(name = "job_resource_generator", sequenceName = JOB_RESOURCE_SEQUENCE, allocationSize = 1)
    @Column(name = ID_COLUMN, updatable = false, nullable = false)
    private var id: Long? = null,

    @Column(name = USER_ID_COLUMN, length = 50)
    var userId: KeycloakUserId? = null,

    @OneToMany(
        mappedBy = JOB_RESOURCE_ATTRIBUTE,
        cascade = [CascadeType.ALL],
        orphanRemoval = true,
        fetch = FetchType.LAZY
    )
    private var _jobs: MutableSet<SystemJob> = HashSet(),

    @JsonIgnore
    @Column(name = URI_COLUMN)
    var uri: URI? = null,

    @Column(name = CREATED_ON_COLUMN, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    var createdOn: Date = Date()
) : Persistable<Long> {


    val jobs: Set<SystemJob>
        @JsonIgnore
        get() = _jobs

    fun addBatchJob(job: SystemJob) {
        _jobs.add(job)
        job.resource = this
    }

    fun removeBatchJob(job: SystemJob) {
        _jobs.remove(job)
        job.resource = null
    }


    override fun getId(): Long? {
        return id
    }

    @JsonIgnore
    @Transient
    override fun isNew() = null == id

    override fun equals(other: Any?): Boolean {
        other ?: return false

        if (this === other) return true

        if (javaClass != ProxyUtils.getUserClass(other)) return false

        other as Persistable<*>

        return if (null == this.getId()) false else this.getId() == other.id
    }

    override fun hashCode(): Int {
        return createdOn.hashCode()
    }

    companion object {
        private const val serialVersionUID = -5554308939380869754L
    }

}