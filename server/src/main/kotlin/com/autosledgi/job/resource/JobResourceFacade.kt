/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.resource

import com.autosledgi.api.rest.JobResourceUploadController
import com.autosledgi.api.security.getUserId
import com.autosledgi.exception.ResourceStorageException
import mu.KLogging
import org.springframework.stereotype.Component
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Path
import java.nio.file.Paths
import java.security.Principal


/**
 * Facade for job resources.
 *
 * @author Jens Buerger
 */
@Component
class JobResourceFacade(
    private val objectRepository: JobResourceObjectRepository,
    private val resourceRepository: JobResourceRepository
) {


    fun createResource(inputStream: InputStream, principal: Principal?): JobResource {
        JobResourceUploadController.logger.debug { "Creating new Job Resource..." }
        val jobResource = resourceRepository.save(JobResource().also { it.userId = getUserId(principal) })
        val uploadPath = getPathForResource(jobResource, principal)
        jobResource.uri = objectRepository.pathToURI(uploadPath)

        return try {
            // copy to object provider
            objectRepository.getOutputStream(uploadPath).use { inputStream.copyTo(it) }
            logger.info { "Successfully uploaded resource bundle ${jobResource.id}" }
            resourceRepository.save(jobResource)
        } catch (e: Exception) {
            resourceRepository.delete(jobResource)
            val wrappedException =
                ResourceStorageException("Exception occurred during resource upload with id # ${jobResource.id}", e)
            logger.warn(wrappedException) { wrappedException.message }
            throw wrappedException
        }
    }

    fun getResourceInputStream(jobResource: JobResource): InputStream {
        val resourceURI = jobResource.uri
        return if (resourceURI != null) {
            objectRepository.getInputStream(resourceURI)
        } else {
            val storageException = ResourceStorageException("Cannot resolve Resource with null URI")
            logger.warn(storageException) { "Exception occurred on resource request" }
            throw storageException
        }
    }


    fun getResource(jobResource: JobResource): OutputStream {
        val resourceURI = jobResource.uri
        return if (resourceURI != null) {
            objectRepository.getOutputStream(resourceURI)
        } else {
            val storageException = ResourceStorageException("Cannot resolve Resource with null URI")
            logger.warn(storageException) { "Exception occurred on resource request" }
            throw storageException
        }
    }



    private fun getPathForResource(jobResource: JobResource, principal: Principal?): Path {
        val resourceId: Long? = jobResource.id
        val principalPath = getUserId(principal)?.uri ?: "SYSTEM"
        return Paths.get("/uploads/$principalPath/$resourceId/resource.zip")
    }


    companion object : KLogging()
}