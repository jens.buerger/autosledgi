/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.batch

import com.autosledgi.job.system.JobStatus
import java.math.BigDecimal
import java.time.Duration

/**
 * Interface for an external Batch System like AWS Batch.
 *
 * @author Jens Buerger
 */
interface BatchProvider {

    suspend fun submitAndRun(batchJob: BatchJob): BatchJob

    suspend fun cancel(batchJob: BatchJob)

    suspend fun terminate(batchJob: BatchJob)

    suspend fun getStatus(batchJob: BatchJob): JobStatus

    /*
        Implementation Note: isFinished should not get the state from the provider, but rather define final states for
        a job
     */
    suspend fun isFinished(batchJob: BatchJob): Boolean

    suspend fun getRuntime(batchJob: BatchJob): Duration?

    suspend fun getCost(batchJob: BatchJob): BigDecimal?
}