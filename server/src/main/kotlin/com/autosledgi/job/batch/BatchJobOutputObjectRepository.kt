/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.batch

import com.autosledgi.persistence.provider.`object`.GenericObjectStorageProvider
import com.autosledgi.persistence.repository.`object`.AbstractObjectStorageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.nio.file.Path
import java.nio.file.Paths

/**
 * Repository for job resources.
 *
 * @author Jens Buerger
 */
@Component
class BatchJobOutputObjectRepository(
    @Autowired @Qualifier(value = "batchJobResultProvider")
    provider: GenericObjectStorageProvider,

    @Value("\${autosledgi.batchresult.bucket}")
    bucket: String?,

    @Value("\${autosledgi.batchresult.pathprefix}")
    pathPrefix: String?
) : AbstractObjectStorageRepository(provider, bucket, pathPrefix) {

    /**
     * @Deprecated
     */
    fun downloadBatchJobResultForId(jobId: Long, targetPath: Path) =
        downloadObject(getPathForBatchJobId(jobId), targetPath)

    fun getInputStreamForId(jobId: Long) = getInputStream(getPathForBatchJobId(jobId))

    fun getPathForBatchJobId(jobId: Long): Path = Paths.get("/$jobId/result.zip")
}