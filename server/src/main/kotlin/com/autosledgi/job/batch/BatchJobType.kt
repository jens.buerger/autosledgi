/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.batch

const val GENERIC_BATCH_JOB_DISCR = "0"
const val MAINTENANCE_BATCH_JOB_DISCR = "1"
const val ISABELLE_SESSION_BATCH_JOB_DISCR = "2"
const val ISABELLE_THEO_PROC_BATCH_JOB_DISCR = "3"

/**
 * Enum class for different autosledgi job types.
 *
 * @author Jens Buerger
 */
enum class BatchJobType(val jobId: Int) {
    GENERIC(GENERIC_BATCH_JOB_DISCR.toInt()),
    MAINTENANCE(MAINTENANCE_BATCH_JOB_DISCR.toInt()),
    ISABELLE_SESSION_BUILD(ISABELLE_SESSION_BATCH_JOB_DISCR.toInt()),
    ISABELLE_THEORY_PROCESSING(ISABELLE_THEO_PROC_BATCH_JOB_DISCR.toInt())
}