/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.batch

import com.autosledgi.job.system.JobStatus
import kotlinx.coroutines.*
import mu.KLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap

private const val BATCH_TERMINATION_TIMEOUT = 30000L

/**
 * Facade for the complete batch system.
 *
 * Besides making sure to forward the jobs to the correct batch provider, it also ensures that the Job DB is consistent.
 *
 * @author Jens Buerger
 */
@Component
class BatchFacade(
    val provider: BatchProvider,
    val jobRepository: BatchJobRepository,
    @Value("\${autosledgi.batch.jobUpdateInterval}")
    private val updateInterval: Int
) {


    private val unfinishedJobs: MutableSet<BatchJob> = ConcurrentHashMap.newKeySet()

    /**
     * Submits, runs and waits for a job
     *
     * @param batchJob The batch job that should be executed.
     */
    suspend fun submitAndRun(batchJob: BatchJob) {
        jobRepository.save(batchJob) // get job id
        provider.submitAndRun(batchJob)
        jobRepository.save(batchJob)
        unfinishedJobs += batchJob
        waitForCompletion(batchJob)
    }


    /**
     * Cancel a batch job, jobs that depend on the canceled job are canceled too.
     * However, if the job is already running it is not terminated.
     *
     * @param batchJob The job that should be canceled.
     */
    suspend fun cancel(batchJob: BatchJob) {
        provider.cancel(batchJob)
        withTimeout(BATCH_TERMINATION_TIMEOUT) {
            waitForCompletion(batchJob)
        }
    }


    /**
     * Terminate a batch job immediately
     *
     * @param batchJob The job that should be terminated.
     */
    suspend fun terminate(batchJob: BatchJob) {
        provider.terminate(batchJob)
        withTimeout(BATCH_TERMINATION_TIMEOUT) {
            waitForCompletion(batchJob)
        }
    }


    /**
     * Update the status of a single batch job
     *
     * @param batchJob The job that should be updated.
     */
    suspend fun updateStatus(batchJob: BatchJob): BatchJob {
        val currentStatus = provider.getStatus(batchJob)
        batchJob.status = currentStatus
        return jobRepository.save(batchJob)
    }


    /*
    this is just a temporary/fallback solution if the batch system does not support notification services
     */
    @Scheduled(fixedRateString = "\${autosledgi.batch.jobUpdateInterval}")
    fun updateUnfinishedJobs() = runBlocking {
        logger.debug { "Updating Batch Job States" }
        for (job in unfinishedJobs.toSet()) {
            launch {
                try {
                    val currentJobStatus = provider.getStatus(job)
                    if (currentJobStatus != job.status) {
                        jobRepository.save(job.also { it.status = currentJobStatus })
                        logger.debug { "Updated job state of # ${job.id} to ${job.status}" }
                        when (job.status) {
                            JobStatus.SUCCEEDED, JobStatus.FAILED -> {
                                logger.info { "Batch Job ${job.id} finished" }
                                if (job.status == JobStatus.FAILED) {
                                    logger.warn { "Batch Job ${job.id} failed!" }
                                }
                                val runtimeUpdate = async { provider.getRuntime(job) }
                                val costUpdate = async { provider.getCost(job) }
                                jobRepository.save(job.also {
                                    it.runtime = runtimeUpdate.await()
                                    it.cost = costUpdate.await()
                                })
                                unfinishedJobs.remove(job)
                            }
                            else -> {
                            }
                        }
                    }
                } catch (e: Exception) {
                    logger.error(e) { "Error occurred while updating job state of # ${job.id} " }
                }
            }

        }
    }

    private suspend fun waitForCompletion(batchJob: BatchJob) {
        while (true) {
            if (provider.isFinished(batchJob)) {
                break
            }
            delay(updateInterval.toLong())
        }
    }

    companion object : KLogging()

}