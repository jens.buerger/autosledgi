/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.batch.jobwrapper

import com.autosledgi.job.batch.BatchJob
import com.autosledgi.job.batch.ISABELLE_SESSION_BATCH_JOB_DISCR
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity

/**
 * Wrapper class for isabelle session build batch jobs.
 *
 * @author Jens Buerger
 */
@Entity
@DiscriminatorValue(value = ISABELLE_SESSION_BATCH_JOB_DISCR)
class IsabelleSessionBuildBatchJob : BatchJob() {
    override fun getJobDefinition(): String = "autosledgi_build_and_run"
}