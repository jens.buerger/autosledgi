/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.batch

import com.autosledgi.common.utils.EMPTY_STRING
import com.autosledgi.job.system.JobPriority
import com.autosledgi.job.system.JobStatus
import com.autosledgi.job.system.SystemJob
import com.fasterxml.jackson.annotation.JsonIgnore
import com.vladmihalcea.hibernate.type.array.StringArrayType
import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import org.hibernate.annotations.TypeDefs
import org.springframework.data.domain.Persistable
import org.springframework.data.util.ProxyUtils
import java.math.BigDecimal
import java.time.Duration
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

const val BATCH_JOB_TABLE = "batch_job"

// sequence
private const val BATCH_JOB_SEQUENCE = "batch_job_sequence"

// table columns
private const val AUTOSLEDGI_JOB_COLUMN = "autosledgi_job"
private const val COST_COLUMN = "cost"
private const val CPU_CORES_COLUMN = "cpu_cores"
private const val MEMORY_COLUMN = "memory"
private const val CREATED_ON_COLUMN = "created_on"
private const val ENVIRONMENT_COLUMN = "environment"
private const val EXTERNAL_ID_COLUMN = "external_id"
private const val ID_COLUMN = "id"
private const val COMMAND_COLUMN = "command"
private const val PRIORITY_COLUMN = "priority"
private const val RUNTIME_COLUMN = "runtime"
private const val STATUS_COLUMN = "status"
private const val TIMEOUT_COLUMN = "timeout"
private const val TYPE_COLUMN = "type"

// foreign attributes
const val AUTOSLEDGI_JOB_ATTRIBUTE = "systemJob"

/**
 * Representation of Batch Jobs
 *
 * @author Jens Buerger
 */
@Entity
@TypeDefs(
    TypeDef(name = "string-array", typeClass = StringArrayType::class),
    TypeDef(name = "jsonb", typeClass = JsonBinaryType::class)
)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name = TYPE_COLUMN,
    discriminatorType = DiscriminatorType.INTEGER
)
@DiscriminatorValue("not null")
@Table(name = BATCH_JOB_TABLE)
abstract class BatchJob(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "batch_job_generator")
    @SequenceGenerator(name = "batch_job_generator", sequenceName = BATCH_JOB_SEQUENCE, allocationSize = 1)
    @Column(name = ID_COLUMN, updatable = false, nullable = false)
    private var id: Long? = null,

    @Column(name = EXTERNAL_ID_COLUMN, length = 60)
    open var externalId: String? = null,

    @Enumerated(EnumType.ORDINAL)
    @Column(name = STATUS_COLUMN, nullable = false)
    open var status: JobStatus = JobStatus.CREATED,

    @Enumerated(EnumType.ORDINAL)
    @Column(name = PRIORITY_COLUMN, nullable = false)
    open var priority: JobPriority = JobPriority.MEDIUM,

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = AUTOSLEDGI_JOB_COLUMN)
    open var systemJob: SystemJob? = null,

    @Type(type = "jsonb")
    @Column(name = ENVIRONMENT_COLUMN, columnDefinition = "jsonb")
    open var environment: MutableMap<String, String> = HashMap(),

    @Type(type = "string-array")
    @Column(name = COMMAND_COLUMN, columnDefinition = "text[]")
    open var command: Array<String> = Array(0) { EMPTY_STRING },

    @Column(name = TIMEOUT_COLUMN, nullable = false)
    open var timeout: Duration = Duration.ofMinutes(10),

    @Column(name = CPU_CORES_COLUMN)
    open var cpuCores: Int? = null,

    @Column(name = MEMORY_COLUMN)
    open var memory: Int? = null,

    @Column(name = RUNTIME_COLUMN)
    open var runtime: Duration? = null,

    @Column(name = COST_COLUMN, precision = 10, scale = 5)
    open var cost: BigDecimal? = null,

    @Column(name = CREATED_ON_COLUMN, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    open var createdOn: Date = Date()

) : Persistable<Long> {


    abstract fun getJobDefinition(): String

    override fun getId(): Long? {
        return id
    }


    // TODO job dependencies

    /**
     * Must be [Transient] in order to ensure that no JPA provider complains because of a missing setter.
     *
     * @see org.springframework.data.domain.Persistable.isNew
     */
    @JsonIgnore
    @Transient
    override fun isNew() = null == id

    override fun equals(other: Any?): Boolean {
        other ?: return false

        if (this === other) return true

        if (javaClass != ProxyUtils.getUserClass(other)) return false

        other as Persistable<*>

        return if (null == this.getId()) false else this.getId() == other.id
    }

    override fun hashCode(): Int {
        return createdOn.hashCode()
    }

    companion object {
        private const val serialVersionUID = -5554308939380869754L
    }

}