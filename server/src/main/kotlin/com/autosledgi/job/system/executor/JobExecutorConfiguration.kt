/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system.executor

import com.autosledgi.exception.AutosledgiJobException
import com.autosledgi.job.system.SystemJob
import com.autosledgi.job.system.jobwrapper.GenericJob
import com.autosledgi.job.system.jobwrapper.TheoryCompletionJob
import org.springframework.stereotype.Component

/**
 * Configuration for the job wrappers i.e. the mapping of job wrappers to job executors.
 *
 * @author Jens Buerger
 */
@Component
class JobExecutorConfiguration(
    private val theoryCompletionExecutor: TheoryCompletionSystemJobExecutor,
    private val genericJobExecutor: GenericJobExecutor
) {

    /*
     * Register job executors here.
     */
    fun getExecutorForJob(systemJob: SystemJob) =
        when (systemJob) {
            is TheoryCompletionJob -> theoryCompletionExecutor
            is GenericJob -> genericJobExecutor
            else -> throw AutosledgiJobException("No compatible job executor registered")
        }
}