/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system.jobwrapper

import com.autosledgi.exception.AutosledgiJobException
import com.autosledgi.job.resource.JobResource
import com.autosledgi.user.KeycloakUserId
import com.autosledgi.user.UserServiceLevel

/**
 * Request class for theory completion system jobs.
 *
 * @author Jens Buerger
 */
data class TheoryCompletionJobRequest(
    val resource: JobResource,
    val mainTheoryName: String,
    val serviceLevel: UserServiceLevel,
    val userId: KeycloakUserId
) {
    init {
        if (mainTheoryName.trim().isEmpty()) {
            throw AutosledgiJobException("Main Theory Name must not be empty")
        }

        if (resource.uri == null) {
            throw AutosledgiJobException("Theory completion without resource not supported")
        }
    }
}