/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system

import com.autosledgi.job.batch.AUTOSLEDGI_JOB_ATTRIBUTE
import com.autosledgi.job.batch.BatchJob
import com.autosledgi.job.resource.JobResource
import com.autosledgi.user.KeycloakUserId
import com.fasterxml.jackson.annotation.JsonIgnore
import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import org.hibernate.annotations.TypeDefs
import org.springframework.data.domain.Persistable
import org.springframework.data.util.ProxyUtils
import java.time.Duration
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

const val SYSTEM_JOB_TABLE = "autosledgi_job"

// table columns
private const val AUTOSLEDGI_JOB_SEQUENCE = "autosledgi_job_sequence"
private const val ID_COLUMN = "id"
private const val STATUS_COLUMN = "status"
private const val PRIORITY_COLUMN = "priority"
private const val TYPE_COLUMN = "type"
private const val RUNTIME_COLUMN = "runtime"
private const val CREATED_ON_COLUMN = "created_on"
private const val USER_COLUMN = "user_id"
private const val JOB_RESOURCE_COLUMN = "resource"
private const val PARAMETER_COLUMN = "parameter"

// foreign attributes
const val JOB_RESOURCE_ATTRIBUTE = "resource"

/**
 * Representation of Autosledgi System Jobs
 *
 * @author Jens Buerger
 */
@Entity
@TypeDefs(
    TypeDef(name = "jsonb", typeClass = JsonBinaryType::class)
)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name = TYPE_COLUMN,
    discriminatorType = DiscriminatorType.INTEGER
)
@DiscriminatorValue("not null")
@Table(name = SYSTEM_JOB_TABLE)
open class SystemJob(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "autosledgi_job_generator")
    @SequenceGenerator(name = "autosledgi_job_generator", sequenceName = AUTOSLEDGI_JOB_SEQUENCE, allocationSize = 1)
    @Column(name = ID_COLUMN, updatable = false, nullable = false)
    private var id: Long? = null,

    @Enumerated(EnumType.ORDINAL)
    @Column(name = STATUS_COLUMN, nullable = false)
    open var status: JobStatus = JobStatus.CREATED,

    @Enumerated(EnumType.ORDINAL)
    @Column(name = PRIORITY_COLUMN, nullable = false)
    open var priority: JobPriority = JobPriority.MEDIUM,

    @Column(name = USER_COLUMN)
    open var userId: KeycloakUserId? = null,

    @OneToMany(
        mappedBy = AUTOSLEDGI_JOB_ATTRIBUTE,
        cascade = [CascadeType.ALL],
        orphanRemoval = true,
        fetch = FetchType.LAZY
    )
    private var _batchJobs: MutableSet<BatchJob> = HashSet(),


    @Type(type = "jsonb")
    @Column(name = PARAMETER_COLUMN, columnDefinition = "jsonb")
    open var parameter: Map<String, String> = HashMap(),

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = JOB_RESOURCE_COLUMN)
    open var resource: JobResource? = null,

    @Column(name = RUNTIME_COLUMN)
    open var runtime: Duration? = null,

    @Column(name = CREATED_ON_COLUMN, columnDefinition = "TIMESTAMP WITH TIME ZONE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    open var createdOn: Date = Date()

) : Persistable<Long> {

    // hide mutability of job jobs
    val batchJobs: Set<BatchJob>
        get() = _batchJobs

    fun addBatchJob(batchJob: BatchJob) {
        _batchJobs.add(batchJob)
        batchJob.systemJob = this
    }

    fun removeBatchJob(batchJob: BatchJob) {
        _batchJobs.remove(batchJob)
        batchJob.systemJob = null
    }


    override fun getId(): Long? {
        return id
    }


    @JsonIgnore
    @Transient
    override fun isNew() = null == id

    override fun equals(other: Any?): Boolean {
        other ?: return false

        if (this === other) return true

        if (javaClass != ProxyUtils.getUserClass(other)) return false

        other as Persistable<*>

        return if (null == this.getId()) false else this.getId() == other.id
    }

    override fun hashCode(): Int {
        return createdOn.hashCode()
    }

    companion object {
        private const val serialVersionUID = -5554308939380869754L
    }

}