/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system.jobwrapper

import com.autosledgi.job.system.SystemJob
import com.autosledgi.job.system.THEORY_COMPLETION_JOB_DISCR
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity


const val MAIN_THEORY_JOB_PARAM = "main_theory"
const val SERVICE_LEVEL_JOB_PARAM = "service_level"

/**
 * Wrapper class for theory completion system jobs.
 *
 * @author Jens Buerger
 */
@Entity
@DiscriminatorValue(value = THEORY_COMPLETION_JOB_DISCR)
class TheoryCompletionJob(request: TheoryCompletionJobRequest) :
    SystemJob(
        userId = request.userId,
        parameter = mapOf(
            MAIN_THEORY_JOB_PARAM to request.mainTheoryName,
            SERVICE_LEVEL_JOB_PARAM to request.serviceLevel.name
        ),
        resource = request.resource
    )