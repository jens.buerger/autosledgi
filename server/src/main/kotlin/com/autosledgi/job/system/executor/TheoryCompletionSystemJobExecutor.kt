/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system.executor

import com.autosledgi.common.isabelle.technical.ProofMechanism
import com.autosledgi.common.isabelle.technical.SledgehammerOptions
import com.autosledgi.common.utils.*
import com.autosledgi.exception.AutosledgiAppException
import com.autosledgi.exception.AutosledgiBusinessException
import com.autosledgi.exception.AutosledgiJobException
import com.autosledgi.isabelle.output.IsabelleServer2018Response
import com.autosledgi.isabelle.output.IsabelleServerOutputParser
import com.autosledgi.isabelle.theory.Lemma
import com.autosledgi.isabelle.theory.ProofStatement
import com.autosledgi.isabelle.theory.Theory
import com.autosledgi.job.batch.BatchFacade
import com.autosledgi.job.batch.BatchJob
import com.autosledgi.job.batch.BatchJobOutputObjectRepository
import com.autosledgi.job.batch.jobwrapper.IsabelleSessionBuildBatchJob
import com.autosledgi.job.batch.jobwrapper.IsabelleTheoProcBatchJob
import com.autosledgi.job.resource.JobResource
import com.autosledgi.job.resource.JobResourceFacade
import com.autosledgi.job.system.JobPriority
import com.autosledgi.job.system.JobStatus
import com.autosledgi.job.system.SystemJob
import com.autosledgi.jobresult.JobResultObjectRepository
import kotlinx.coroutines.*
import mu.KLogging
import org.springframework.stereotype.Component
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.time.Duration
import java.util.zip.ZipEntry



const val DEF_CPU_CORES_BATCH_JOB_SETTING = 4
const val DEF_MEMORY_BATCH_JOB_SETTING = 6000
const val DEF_COMMAND_BATCH_JOB_SETTING = "isabelle_cloud_build.sh"
const val DEF_CPU_NUMBER_BATCH_JOB_ENV = "4"

const val JOB_RESOURCE_LOC_ENV_VAR = "JOB_RESOURCE_LOCATION"
const val CUSTOMER_ID_ENV_VAR = "CUSTOMER_ID"
const val CPU_NUM_ENV_VAR = "CPU_NUMBER"
const val MAIN_THEORY_ENV_VAR = "RUNNER_THEORY"
const val ISABELLE_VERSION_ENV_VAR = "ISABELLE_VERSION"
const val MAIN_SESSION_ENV_VAR = "RUNNER_SESSION"


/**
 * Executor for theory completion jobs.
 *
 * @author Jens Buerger
 */
@Component
class TheoryCompletionSystemJobExecutor(
    private val batchFacade: BatchFacade,
    private val batchOutputRepository: BatchJobOutputObjectRepository,
    private val jobResourceFacade: JobResourceFacade,
    private val jobResultObjectRepository: JobResultObjectRepository,
    private val isabelleServerOutputParser: IsabelleServerOutputParser

) : SystemJobExecutor {

    var executorCoroutine: Job? = null

    override suspend fun terminate(job: SystemJob) {
        logger.debug { "Going to terminate Theory Completion Job ${job.id}" }
        coroutineScope {
            job.batchJobs.filter { it.id != null }.forEach {
                val terminateCoroutine = async { batchFacade.terminate(it) }
                terminateCoroutine.await()
                executorCoroutine!!.cancelChildren()
                executorCoroutine!!.cancelAndJoin()
                logger.debug { "Finished terminating" }
            }

        }
    }

    // helper class needed because of error handling
    data class BatchJobWithSorryIndex(val batchJob: BatchJob, val sorryIndex: Int)

    override suspend fun execute(job: SystemJob): JobStatus {
        val mainTheory = readMainTheoryFile(job)
        val unfinishedLemma = mainTheory.lemmas.filter { it.unfinished }
        logger.debug { "Detected ${unfinishedLemma.size} unfinished lemma" }

        if (unfinishedLemma.isEmpty()) {
            return JobStatus.SUCCEEDED
        } else if (sessionBuildRequired(job)) {
            submitSessionBuildJobAndWait(job)
        }

        // create and submit batch jobs
        val sorryJobForLemma: MutableMap<Lemma, MutableList<BatchJobWithSorryIndex>> =
            createBatchJobsForAllSorries(job, mainTheory, unfinishedLemma)
        submitAllBatchJobsAndWait(sorryJobForLemma)

        // update theory file from batch results
        updateTheoryFromBatchResults(sorryJobForLemma)

        // update file and upload
        mainTheory.updateAllLemmasInTextFile()
        uploadCompletedTheory(job, mainTheory)
        return JobStatus.SUCCEEDED
    }


    private fun updateLemmaFromBatchResult(lemma: Lemma, sorryIndex: Int, batchJob: BatchJob) {
        val isabelleOutput: MutableList<IsabelleServer2018Response> = ArrayList()
        try {
            val inputStream =
                cloneInputStream(
                    batchOutputRepository.getInputStreamForId(
                        batchJob.id ?: throw AutosledgiAppException("Job id null")
                    )
                )
            processEntryFromZipInputStream(inputStream, "session_log") {
                isabelleOutput.addAll(isabelleServerOutputParser.parseStream(it))
            }
        } catch (e: Exception) {
            logger.error(e) { "Could not parse isabelle output of batch job # ${batchJob.id}" }
        }

        val foundProofStatements: Set<ProofStatement> = try {
            isabelleServerOutputParser.getProofStatementsFromSledgehammerOutput(isabelleOutput)
        } catch (e: Exception) {
            logger.error(e) { "Exception occurred during proof extraction" }
            emptySet()
        }

        val selectedProofStatement = prioritizeAndSelectProof(foundProofStatements)

        if (selectedProofStatement != null) {
            // isabelle found a viable proof so we can inject it
            logger.debug { "Injected proof for ${lemma.name} / $sorryIndex" }
            lemma.proof?.injectProofStatement(sorryIndex, selectedProofStatement)
        } else {
            logger.debug { "CHANGEME: Could not find a viable proof statement!" }
            lemma.proof?.injectString(sorryIndex, "\nsorry (* Autosledgi: Proof Not found *)\n")
        }
    }

    private fun prioritizeAndSelectProof(proofStatements: Set<ProofStatement>): ProofStatement? {
        logger.debug { "Prioritizing ${proofStatements.size} proof statements" }
        val finishedProofs = proofStatements.filter { it.duration != null }
        finishedProofs.sortedBy { it.proofMechanisms.maxBy(ProofMechanism::desirability) }
        logger.debug { "Found ${finishedProofs.size} proofs that terminated" }
        if (finishedProofs.isNotEmpty()) {
            return finishedProofs.first()
        }
        return null
    }

    private fun getMainTheory(job: SystemJob) =
        job.parameter["main_theory"] ?: throw AutosledgiAppException("Main theory name not present")

    private fun getMainTheoryForBatchJob(job: SystemJob) = getMainTheory(job).removeSuffix(".thy")

    private fun readMainTheoryFile(job: SystemJob): Theory {
        logger.debug { "Loading main theory file ${job.parameter["main_theory"]} from job resources..." }
        val jobResource =
            job.resource ?: throw AutosledgiBusinessException("Theory completion jobs require a job resource")
        val inputStream = cloneInputStream(jobResourceFacade.getResourceInputStream(jobResource))

        val mainTheoryName = getMainTheory(job)
        var mainTheory: Theory? = null
        processEntryFromZipInputStream(inputStream, mainTheoryName) {
            mainTheory = Theory(it)
        }

        if (mainTheory == null) {
            throw AutosledgiBusinessException("Main theory file not present in job resource")
        } else {
            return mainTheory as Theory
        }
    }


    private suspend fun submitSessionBuildJobAndWait(job: SystemJob) {
        logger.debug { "Submitting session build jobs for job # ${job.id}" }
        val sessionBuildJob = IsabelleSessionBuildBatchJob().also {
            it.cpuCores = DEF_CPU_CORES_BATCH_JOB_SETTING
            it.memory = DEF_MEMORY_BATCH_JOB_SETTING
            it.command = arrayOf(DEF_COMMAND_BATCH_JOB_SETTING)
            it.environment = mapOf(
                JOB_RESOURCE_LOC_ENV_VAR to job.resource!!.uri.toString(),
                CUSTOMER_ID_ENV_VAR to (job.userId?.uri ?: "nobody"),
                CPU_NUM_ENV_VAR to DEF_CPU_NUMBER_BATCH_JOB_ENV,
                MAIN_THEORY_ENV_VAR to getMainTheoryForBatchJob(job),
                ISABELLE_VERSION_ENV_VAR to "2018",
                MAIN_SESSION_ENV_VAR to "fun" // TODO check if this is required
            ).toMutableMap()
            it.timeout = Duration.ofMinutes(10)
            it.priority = JobPriority.HIGHEST
        }
        job.addBatchJob(sessionBuildJob)
        coroutineScope {
            val buildCoroutine = async { batchFacade.submitAndRun(sessionBuildJob) }
            buildCoroutine.await()
            batchFacade.updateStatus(sessionBuildJob)
            if (sessionBuildJob.status != JobStatus.SUCCEEDED) {
                throw AutosledgiBusinessException("Session builds failed")
            }
        }

    }


    private suspend fun createBatchJobForSorry(
        job: SystemJob,
        mainTheory: Theory,
        lemma: Lemma,
        sorryIndex: Int
    ): BatchJob {
        val sorryJobResource = createResourceForSorryJob(job, mainTheory, lemma, sorryIndex)
        return IsabelleTheoProcBatchJob().also {
            it.cpuCores = DEF_CPU_CORES_BATCH_JOB_SETTING
            it.memory = DEF_MEMORY_BATCH_JOB_SETTING
            it.command = arrayOf(DEF_COMMAND_BATCH_JOB_SETTING)
            it.environment = mapOf(
                JOB_RESOURCE_LOC_ENV_VAR to sorryJobResource.uri.toString(),
                CUSTOMER_ID_ENV_VAR to (job.userId?.uri ?: "nobody"),
                CPU_NUM_ENV_VAR to DEF_CPU_NUMBER_BATCH_JOB_ENV,
                MAIN_THEORY_ENV_VAR to getMainTheoryForBatchJob(job),
                ISABELLE_VERSION_ENV_VAR to "2018",
                MAIN_SESSION_ENV_VAR to "fun" // TODO check if this is required
            ).toMutableMap()
            it.timeout = Duration.ofMinutes(10)
            it.priority = JobPriority.HIGHEST
        }
    }

    private fun createResourceForSorryJob(
        job: SystemJob,
        mainTheory: Theory,
        lemma: Lemma,
        sorryIndex: Int
    ): JobResource {
        val theoryCopy = mainTheory.getDeepCopy()
        val lemmaCopy: Lemma = mainTheory.getLemmaSiblingInCopy(theoryCopy, lemma)
            ?: throw AutosledgiJobException("Could not identify lemma sibling")
        val proofCopy = lemmaCopy.proof!!
        proofCopy.injectSledgehammerCommand(sorryIndex, SledgehammerOptions())
        proofCopy.updateFile()

        val originalJobResource = job.resource ?: throw AutosledgiAppException("Job resource must not be null")
        val originalJobResourceStream = jobResourceFacade.getResourceInputStream(originalJobResource)
        val mainTheoryName = getMainTheory(job)
        val tmpByteOutputStream = ByteArrayOutputStream()

        zipFileMap(originalJobResourceStream, tmpByteOutputStream) {
            if (it.entry.name == mainTheoryName) {
                // we are currently selecting the main theory file
                ZipEntryWithStream(theoryCopy.getInputStream(), ZipEntry(it.entry.name))
            } else {
                it
            }
        }

        val tmpByteInputStream = ByteArrayInputStream(tmpByteOutputStream.toByteArray())
        return jobResourceFacade.createResource(tmpByteInputStream, null)
    }

    private suspend fun submitAllBatchJobsAndWait(sorryJobForLemma: MutableMap<Lemma, MutableList<BatchJobWithSorryIndex>>) {
        logger.debug { "Submitting theory processing build jobs" }
        val submissionResults: MutableList<Deferred<Unit>> = ArrayList()
        coroutineScope {
            sorryJobForLemma.forEach { _, batchJobs ->
                batchJobs.forEach { submissionResults.add(async { batchFacade.submitAndRun(it.batchJob) }) }
            }
            submissionResults.forEach { it.await() }
        }
    }


    private suspend fun createBatchJobsForAllSorries(
        job: SystemJob,
        mainTheory: Theory,
        unfinishedLemma: List<Lemma>
    ): MutableMap<Lemma, MutableList<BatchJobWithSorryIndex>> {
        val sorryJobForLemma: MutableMap<Lemma, MutableList<BatchJobWithSorryIndex>> = HashMap()
        unfinishedLemma.forEach { lemma ->
            val proof = lemma.proof ?: throw AutosledgiBusinessException("Proof empty!")
            for (sorryIndex in 0 until proof.sorries) {
                val sorryJob = createBatchJobForSorry(job, mainTheory, lemma, sorryIndex)
                job.addBatchJob(sorryJob)
                addElementToLemmaMap(sorryJobForLemma, lemma, BatchJobWithSorryIndex(sorryJob, sorryIndex))
            }
        }
        return sorryJobForLemma
    }

    @Suppress("UNUSED_PARAMETER")
    fun sessionBuildRequired(job: SystemJob): Boolean {
        return true
    }

    private suspend fun uploadCompletedTheory(job: SystemJob, mainTheory: Theory) {
        logger.debug { "Uploading finished theory for job # ${job.id}" }
        val outputStream = jobResultObjectRepository.getOutputStreamForId(job.id!!)
        val outputMainTheoryName = "${mainTheory.name}.thy"
        val mainTheoryEntry = ZipEntryWithStream(mainTheory.getInputStream(), ZipEntry(outputMainTheoryName))
        entriesToZipStream(listOf(mainTheoryEntry), outputStream)
    }

    private suspend fun updateTheoryFromBatchResults(sorryJobForLemma: MutableMap<Lemma, MutableList<BatchJobWithSorryIndex>>) {
        logger.debug { "Updating main theory from batch results" }
        sorryJobForLemma.forEach { lemma, sorryJobs ->
            sorryJobs.forEach { (job, sorryIndex) ->
                if (job.status == JobStatus.SUCCEEDED) {
                    logger.debug { "Using Batch job ${job.id} results" }
                    updateLemmaFromBatchResult(lemma, sorryIndex, job)
                } else {
                    logger.debug { "Batch job # ${job.id} results skipped" }
                }
            }
        }
    }

    private fun <T> addElementToLemmaMap(lemmaMap: MutableMap<Lemma, MutableList<T>>, lemma: Lemma, element: T) {
        lemmaMap[lemma] = (lemmaMap[lemma] ?: ArrayList()).also { it.add(element) }
    }

    companion object : KLogging()
}