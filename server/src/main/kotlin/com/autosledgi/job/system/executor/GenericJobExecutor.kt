/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system.executor

import com.autosledgi.job.system.JobStatus
import com.autosledgi.job.system.SystemJob
import com.autosledgi.job.system.jobwrapper.INITIAL_DELAY_PARAM
import kotlinx.coroutines.delay
import mu.KLogging
import org.springframework.stereotype.Component
import java.time.Duration

/**
 * Executor for theory completion jobs.
 *
 * @author Jens Buerger
 */
@Component
class GenericJobExecutor : SystemJobExecutor {

    override suspend fun terminate(job: SystemJob) {
        // do nothing
    }

    override suspend fun execute(job: SystemJob): JobStatus {
        val initialDelay: Duration = Duration.ofMillis(job.parameter[INITIAL_DELAY_PARAM]?.toLong() ?: 0)
        delay(initialDelay.toMillis())

        return JobStatus.SUCCEEDED
    }

    companion object : KLogging()
}