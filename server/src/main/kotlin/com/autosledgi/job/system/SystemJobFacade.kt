/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system

import com.autosledgi.exception.AutosledgiJobException
import com.autosledgi.job.system.executor.JobExecutorConfiguration
import com.autosledgi.job.system.executor.SystemJobExecutor
import kotlinx.coroutines.*
import mu.KLogging
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.stereotype.Component
import java.time.Duration
import java.time.Instant
import java.util.*
import java.util.concurrent.ConcurrentHashMap


/**
 * Facade for the autosledgi job system.
 *
 * @author Jens Buerger
 */
@Component
class SystemJobFacade(
    private val jobRepository: SystemJobRepository,
    private val jobExecutorConfiguration: JobExecutorConfiguration
) {

    private val executorService = ThreadPoolTaskExecutor().also {
        it.maxPoolSize = 100
        it.setQueueCapacity(0)
        it.initialize()
    }

    private val autosledgiJobCoroutine = executorService.asCoroutineDispatcher()

    //save coroutine for autosledgi job execution
    private val coroutineForJob: MutableMap<SystemJob, Job> = ConcurrentHashMap()


    /**
     * Launch the complete job processing and update all states and costs.
     *
     * As the function returns a Job object one can easily wait on jobs using the join() function
     *
     * @param systemJob The autosledgi job id that represents the processing job.
     * @return The coroutine that executes the job which allows joins etc.
     */
    fun launch(systemJob: SystemJob): Job {
        jobRepository.save(systemJob.also { it.status = JobStatus.RUNNING })
        val executor: SystemJobExecutor = jobExecutorConfiguration.getExecutorForJob(systemJob)
        // TODO OPT choose different thread pool depending on priority
        val job = GlobalScope.launch(autosledgiJobCoroutine) {
            logger.debug { "Launching autosledgi job # ${systemJob.id} with ${systemJob.priority}" }
            val startTime = Instant.now()
            try {
                systemJob.status = executor.execute(systemJob)
            } catch (cancelEx: CancellationException) {
                // this is executed when the job is canceled
                systemJob.status = JobStatus.TERMINATED
                logger.debug { "Invoking termination function of autosledgi job # ${systemJob.id}" }
                executor.terminate(systemJob)
            } catch (e: Exception) {
                systemJob.status = JobStatus.FAILED
                logger.warn(e) { "Unhandled Exception occurred during execution of job # ${systemJob.id}" }
                throw e
            } finally {
                logger.info { "Executing finally" }
                systemJob.runtime = Duration.between(startTime, Instant.now())
                jobRepository.save(systemJob)
                coroutineForJob.remove(systemJob)
            }
        }

        coroutineForJob[systemJob] = job
        return job
    }


    /**
     * Terminate the complete job processing and update all states and costs.
     *
     * Note: join the returned coroutine if you want to make sure that the job is terminated after the call
     *
     * @param systemJob The autosledgi job id that represents the processing job.
     */
    fun terminate(systemJob: SystemJob): Job {
        logger.debug { "Trying to terminate job with id # ${systemJob.id}" }
        val job = coroutineForJob[systemJob] ?: throw AutosledgiJobException("Job coroutine could not be resolved")
        return try {
            // TODO OPT also cancel batch jobs here
            GlobalScope.launch(autosledgiJobCoroutine) {
                job.cancel()
                job.join()
                logger.debug { "Termination of autosledgi job # ${systemJob.id} successful" }
            }
        } catch (e: Exception) {
            throw AutosledgiJobException("Could not terminate autosledgi job # ${systemJob.id}", e)
        }
    }


    /**
     * Return the current state of an isabelle processing job.
     *
     * @param jobId The autosledgi job id that represents the processing job.
     */
    fun getJobById(jobId: Long): Optional<SystemJob> = jobRepository.findById(jobId)


    companion object : KLogging()

}