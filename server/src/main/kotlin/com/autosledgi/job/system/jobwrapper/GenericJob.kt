/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system.jobwrapper

import com.autosledgi.job.system.GENERIC_JOB_DISCR
import com.autosledgi.job.system.SystemJob
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity

const val INITIAL_DELAY_PARAM = "initial_delay"

/**
 * Wrapper class for generic system jobs.
 *
 * @author Jens Buerger
 */
@Entity
@DiscriminatorValue(value = GENERIC_JOB_DISCR)
class GenericJob(request: GenericJobRequest) :
    SystemJob(
        parameter = mapOf(INITIAL_DELAY_PARAM to request.initialDelay?.toMillis().toString())
    )