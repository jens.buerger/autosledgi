-- remove or update this meaningless migration script when we enable the database

CREATE SEQUENCE hibernate_sequence;

/*-- user management
CREATE TABLE autosledgi_user
(
  id            BIGINT,
  username      VARCHAR(50) UNIQUE,
  service_level INTEGER NOT NULL,
  created_on    TIMESTAMP WITH TIME ZONE,
  PRIMARY KEY (id)
);

CREATE SEQUENCE autosledgi_user_sequence

  START WITH 1;
--*/


-- job resources
CREATE TABLE job_resource
(
  id         BIGINT,
  user_id    VARCHAR(50),
  uri        TEXT,
  created_on TIMESTAMP WITH TIME ZONE NOT NULL,
  PRIMARY KEY (id)
);

CREATE SEQUENCE autosledgi_job_resource_sequence
  START WITH 1;
--


-- autosledgi system jobs
CREATE TABLE autosledgi_job
(
  id         BIGINT,
  status     INTEGER                  NOT NULL,
  priority   INTEGER                  NOT NULL,
  user_id    VARCHAR(50),
  type       INTEGER                  NOT NULL,
  resource   BIGINT,
  runtime    BIGINT,
  parameter  JSONB,
  created_on TIMESTAMP WITH TIME ZONE NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (resource) REFERENCES job_resource (id)
);

CREATE SEQUENCE autosledgi_job_sequence
  START WITH 1;
--


-- batch system jobs
CREATE TABLE batch_job
(
  id             BIGINT,
  external_id    VARCHAR(50),
  status         INTEGER                  NOT NULL,
  priority       INTEGER                  NOT NULL,
  autosledgi_job BIGINT,
  type           INTEGER                  NOT NULL,
  timeout        BIGINT                   NOT NULL,
  cpu_cores      INTEGER,
  memory         INTEGER,
  runtime        BIGINT,
  cost           NUMERIC(10, 5),
  created_on     TIMESTAMP WITH TIME ZONE NOT NULL,
  environment    JSONB,
  command        text[],
  PRIMARY KEY (id),
  FOREIGN KEY (autosledgi_job) REFERENCES autosledgi_job (id)
);

CREATE SEQUENCE batch_job_sequence
  START WITH 1;
--
