theory test

imports Main

begin

theorem blub: assumes "x = 2"
  shows "x = 2"
  by (simp add: assms)

lemma Hallo: "x = x"
  by (simp)

end