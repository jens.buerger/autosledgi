theory Test

(* some comment *)

imports Main

begin
(*OPTIONS: test *)
lemma easy: "1 + 1 = 2"
  by (simp)

lemma harder : "3 + 4 = 7 \<and> 1 + 2 = 3"
  (* bla bla *)
  apply(rule)
  apply (auto[1])
  by (simp)
 text {* Convert an event-spf to a timed-spf. Just a restriction of the function domain. *}

lemma isar:  "5 + 6 = 11 \<and> 1 + 2 = 3"
proof -have"5+ 6 = 11"proof-show ?thesis by (simp)
  qed
  moreover have "1 +2 =3"
    by (simp)
  show ?thesis
    by (simp)
qed

lemma "1 = 1"
  by (simp)

lemma bla: assumes "True" and "False"
  shows "True"
  apply (simp)

end