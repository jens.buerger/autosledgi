/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.output

import com.autosledgi.common.utils.processEntryFromZipInputStream
import com.autosledgi.isabelle.theory.ProofStatement
import com.autosledgi.job.batch.BatchJobOutputObjectRepository
import mu.KLogging
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import java.io.ByteArrayInputStream


@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
@Transactional
internal class OutputParserProofStatementsTest{


    @Autowired
    lateinit var batchOutputRepository: BatchJobOutputObjectRepository
    @Autowired
    lateinit var isabelleServerOutputParser: IsabelleServerOutputParser

    //@Ignore
    @Test
    fun `unzip and parse result zip from S3`() {

        val path = batchOutputRepository.getPathForBatchJobId(53)
        val fileName = "session_log"
        var proofStatements: Set<ProofStatement> = emptySet()
        processEntryFromZipInputStream(batchOutputRepository.getInputStream(path), fileName) {
            val inputAsString = it.bufferedReader().readText()
            val stream = ByteArrayInputStream(inputAsString.toByteArray(Charsets.UTF_8))
            proofStatements = isabelleServerOutputParser.getProofStatementsFromSledgehammerOutput(
                isabelleServerOutputParser.parseStream(
                    stream
                )
            )
        }
        println(proofStatements)

    }


    companion object : KLogging()
}