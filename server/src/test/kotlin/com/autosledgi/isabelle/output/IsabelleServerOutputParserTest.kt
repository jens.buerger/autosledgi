/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.output

import io.kotlintest.matchers.boolean.shouldBeFalse
import io.kotlintest.matchers.collections.shouldHaveAtLeastSize
import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.matchers.string.shouldContain
import io.kotlintest.matchers.types.shouldBeInstanceOf
import io.kotlintest.shouldBe
import org.junit.jupiter.api.Test
import java.nio.file.Paths

/**
 * Class for testing the server output parser
 *
 * @author Pascal Bontempi
 */
internal class IsabelleServerOutputParserTest {

    private val isabelleServerOutputParser = IsabelleServerOutputParser()
    private val testResponse: List<IsabelleServer2018Response> = isabelleServerOutputParser.loadAndParseFile(TEST_DATA)

    @Test
    fun `checks if parsing generally works`() {
        testResponse.shouldHaveSize(6)
        testResponse[0].task.shouldBe("91b28ba8-7cf3-4a5b-b7c0-009bda6f01d4")
        (testResponse[1] as IsabelleServer2018FailedResponse).kind.shouldBe("error")
        testResponse[2].task.shouldBe("d573e206-e3e6-491d-a6d8-5e65037bb7f7")
        (testResponse[3] as IsabelleServer2018NoteResponse).message.shouldContain("Building Main")
        testResponse.last().shouldBeInstanceOf<IsabelleServer2018FinishedResponse>()
    }

    @Test
    fun `checks if output structure is fine`() {

        // There should be at least an OK or FAILED, a NOTE and a FINISHED response
        testResponse.shouldHaveAtLeastSize(3)

        // each server outputs last message should be type of FINISHED
        testResponse.last().shouldBeInstanceOf<IsabelleServer2018FinishedResponse>()

        // task of the first response should match the task of the last response
        testResponse.last().task.shouldBe(testResponse.first().task)

        // if there is a FAILED message then the FINISHED ok-attribute should be false
        if (testResponse.filterIsInstance<IsabelleServer2018FailedResponse>().isNotEmpty()) {
            (testResponse.last() as IsabelleServer2018FinishedResponse).ok.shouldBeFalse()
        }
    }

    companion object {
        private val PARSER_TEST_DATA_DIR = Paths.get(System.getProperty("user.dir") + "/src/test/resources/outputParserTestFiles/")
        private val TEST_DATA = PARSER_TEST_DATA_DIR.resolve("test.txt")
    }
}