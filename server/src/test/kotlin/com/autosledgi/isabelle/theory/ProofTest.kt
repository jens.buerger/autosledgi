/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import com.autosledgi.common.isabelle.technical.ProofMechanism
import com.autosledgi.common.isabelle.technical.isabelleNormalize
import com.autosledgi.common.utils.splitLines
import com.nhaarman.mockitokotlin2.doNothing
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.kotlintest.data.forall
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.tables.row
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

internal class ProofTest {


    @Test
    fun `check if texts are equal for isabelle (no pretty printing necessary)`() {
        ISAR_PROOF.text.isabelleNormalize().map { it.trim() } shouldBe
                splitLines(isarProofRawText.isabelleNormalize()).map { it.trim() }
    }

    // TODO also check for pretty printing

    @Test
    fun `parent update with parent not present`() {
        shouldThrow<IllegalStateException> { ProofStatementTest.proofStatementUf.updateFile() }
    }


    @Test
    fun `parent update with parent present`() {
        val mockProof = mock<Proof>()
        doNothing().whenever(mockProof).updateFile()

        val proofStatementOops = ProofStatement("oops", mockProof)
        proofStatementOops.updateFile()

        verify(mockProof).updateFile()
    }


    @Test
    fun `check unfinished delegation`() {
        forall(
            row(ISAR_PROOF, false),
            row(AUTOSIMP_PROOF, false),
            row(SIMP_PROOF, false),
            row(ISAR_SORRY_PROOF, true)
        ) { proof, exp -> proof.unfinished shouldBe exp }
    }


    @Test
    fun `get sum of sorries`() {
        forall(
            row(ISAR_PROOF, 0),
            row(AUTOSIMP_PROOF, 0),
            row(SIMP_PROOF, 0),
            row(ISAR_SORRY_PROOF, 3)
        ) { proof, exp -> proof.sorries shouldBe exp }
    }


    @Test
    fun `get proof mechanisms`() {
        forall(
            row(ISAR_PROOF, setOf(ProofMechanism.ISAR, ProofMechanism.SIMP)),
            row(AUTOSIMP_PROOF, setOf(ProofMechanism.AUTO, ProofMechanism.SIMP)),
            row(SIMP_PROOF, setOf(ProofMechanism.SIMP)),
            row(ISAR_SORRY_PROOF, setOf(ProofMechanism.ISAR, ProofMechanism.SORRY, ProofMechanism.SIMP))
        ) { proof, exp -> proof.proofMechanisms shouldBe exp }
    }

    @Disabled
    @Test
    fun openSubgoals() {
    }

    @Disabled
    @Test
    fun injectSledgehammerCommand() {
    }


    @Test
    fun `check number of statements after parsing`() {
        forall(
            row(ISAR_PROOF, 1),
            row(AUTOSIMP_PROOF, 3),
            row(SIMP_PROOF, 1),
            row(ISAR_SORRY_PROOF, 2)
        ) { proof, exp -> proof.statements.size shouldBe exp }
    }


    @Test
    fun `check statements in detail after simple parsing`() {
        val rawText = """
            apply (simp)
            sorry
        """.trimIndent()
        val proof = Proof(rawText, null)
        proof.statements.size shouldBe 2
        proof.statements shouldBe listOf(
            ProofStatement("apply (simp)", null),
            ProofStatement("sorry", null)
        )
    }

    companion object {
        private val isarProofRawText = """
            proof -
              have "5+ 6 = 11"
                proof -
                show ?thesis by (simp)
              qed
              moreover have "1 +2 =3"
                by (simp)
              show ?thesis
                by (simp)
            qed
        """.trimIndent()
        val ISAR_PROOF = Proof(isarProofRawText, null)

        private val autoSimpRawText = """
            apply(rule)
            apply (auto[1])
            by (simp)
        """.trimIndent()
        val AUTOSIMP_PROOF = Proof(autoSimpRawText, null)

        private const val simpRawText = "by (simp)"
        val SIMP_PROOF = Proof(simpRawText, null)

        private val isarWithSorriesText = """
            proof -
              have "5+ 6 = 11"
                proof -
                show ?thesis sorry
              qed
              moreover have "1 +2 =3"
                sorry
              show ?thesis
                by (simp)
            qed
            sorry
        """.trimIndent()
        val ISAR_SORRY_PROOF = Proof(isarWithSorriesText, null)
    }
}