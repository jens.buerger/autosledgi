/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import com.autosledgi.common.isabelle.technical.ProofMechanism
import com.autosledgi.common.isabelle.technical.SledgehammerOptions
import com.autosledgi.common.utils.concatLines
import com.nhaarman.mockitokotlin2.doNothing
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.kotlintest.data.forall
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.tables.row
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test


internal class ProofStatementTest {


    @Test
    fun `sorry detection`() {
        proofStatementUf.unfinished shouldBe true
        proofStatementF.unfinished shouldBe false
    }

    @Test
    fun `check number of sorries`() {
        forall(
            row(proofStatementUf, 1),
            row(proofStatementUf2, 3),
            row(proofStatementF, 0)
        ) { statement, sorries -> statement.sorries shouldBe sorries }
    }


    @Test
    fun oops() {
        val proofStatementNoOops = ProofStatement("apply (simpoops)", null)
        forall(
            row(proofStatementOops, true),
            row(proofStatementNoOops, false),
            row(proofStatementUf, false)
        )
        { statement, isOops -> statement.oops shouldBe isOops }
    }

    //TODO add proofs for

    @Disabled
    @Test
    fun `open subgoals`() {
        TODO("implement this test when Isabelle API is ready")
    }

    @Disabled
    @Test
    fun `inject sledgehammer command`() {
        TODO("implement this test when Isabelle API is ready")
    }


    @Test
    fun `parent update with parent not present`() {
        shouldThrow<IllegalStateException> { proofStatementUf.updateFile() }
    }

    @Test
    fun `parent update with parent present`() {
        val mockProof = mock<Proof>()
        doNothing().whenever(mockProof).updateFile()

        val proofStatementOops = ProofStatement("oops", mockProof)
        proofStatementOops.updateFile()

        verify(mockProof).updateFile()
    }


    @Test
    fun `proof mechanisms`() {
        forall(
            row(proofStatementUf, setOf(ProofMechanism.SORRY, ProofMechanism.SIMP)),
            row(proofStatementF, setOf(ProofMechanism.SIMP)),
            row(proofStatementOops, setOf(ProofMechanism.OOPS))
        ) { statement, mechanisms -> statement.proofMechanisms shouldBe mechanisms }
    }


    @Test
    fun `check inject sledgehammer`() {
        val statement = ProofStatement(listOf("sorry", "sorry").toMutableList(), null)
        statement.injectSledgehammerCommand(1, SledgehammerOptions())
        println(statement.proofStatements.concatLines())
    }

    @Test
    fun `text generation from simple raw text`() {
        val statement = ProofStatement("       apply (simp) \n sorry", null)
        statement.text shouldBe listOf("apply (simp)", "sorry")
    }

    companion object {
        val proofStatementUf = ProofStatement("apply (simp) \n sorry", null)
        val proofStatementUf2 = ProofStatement("applysorry (simp) \n sorry sorry sorry", null)
        val proofStatementF = ProofStatement("apply (simp) \n by simp", null)
        val proofStatementOops = ProofStatement("oops", null)
    }

}