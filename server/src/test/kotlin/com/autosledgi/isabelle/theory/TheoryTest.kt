/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import com.autosledgi.common.utils.concatLines
import io.kotlintest.data.forall
import io.kotlintest.shouldBe
import io.kotlintest.tables.row
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.nio.file.Files
import java.nio.file.Paths

internal class TheoryTest {

    private lateinit var empty: Theory
    private lateinit var full: Theory
    private lateinit var fullOneRemoved: Theory
    private lateinit var fullOneAdded: Theory
    private lateinit var fullMoved: Theory


    @BeforeEach
    fun setup() {
        empty = Theory(Paths.get("$DIR/Empty.thy"))
        full = Theory(Paths.get("$DIR/Full.thy"))
        fullOneRemoved = Theory(Paths.get("$DIR/Full_OneRemoved.thy"))
        fullOneAdded = Theory(Paths.get("$DIR/Full_OneAdded.thy"))

        // Directory for the renaming test
        Paths.get(RENAME_DIR).toFile().mkdir()
        Files.copy(Paths.get("$DIR/Full.thy"), Paths.get("$RENAME_DIR/Full.thy"))
        fullMoved = Theory(Paths.get("$RENAME_DIR/Full.thy"))
    }


    @Test
    fun `check theory name and import parsing with empty theory`() {
        empty.imports.all { it.theoryName == "Main" } shouldBe true
        empty.lemmas.size shouldBe 0
    }


    @Test
    fun `check parsing of lemma_test theory`() {
        full.imports.size shouldBe 2
        full.imports.all { it.theoryName == "Main" || it.theoryName == "Foo" } shouldBe true
        full.lemmas.size shouldBe 5
    }


    @Test
    fun `remove lemma from theory`() {
        full.removeLemma(full.lemmas[3]) shouldBe true
        full.lemmas.size shouldBe 4
        full.contents.concatLines() shouldBe fullOneRemoved.contents.concatLines()
    }


    @Test
    fun `add lemma to theory`() {
        full.addLemma(full.lemmas[3])
        full.lemmas.size shouldBe 6
        full.contents.concatLines() shouldBe fullOneAdded.contents.concatLines()
    }


    @Disabled
    @Test
    fun updateOuterTheoryComponentRegion() {
    }


    @Disabled
    @Test
    fun getName() {
        forall(
            row(empty, "Empty"),
            row(full, "Full")
        ) { theory, name -> theory.name shouldBe name }
    }


    @Test
    fun setName() {
        fullMoved.name = "Moved"

        fullMoved.name shouldBe "Moved"
        fullMoved.contents[0] shouldBe "theory Moved"
        fullMoved.contents.size shouldBe full.contents.size
        val content = Files.readAllLines(fullMoved.path, fullMoved.fileCharset)
        content.size shouldBe full.contents.size
        content[0] shouldBe "theory Moved"
        for (i in 1 until fullMoved.contents.size) {
            content[i] shouldBe full.contents[i]
        }
    }

    @Test
    fun `check lemma copy sibling resolution`() {
        val fullCopy = full.getDeepCopy()
        val copyEasyLemma = fullCopy.lemmas.first { it.name == "easy" }
        val easyLemma = full.lemmas.first { it.name == "easy" }
        full.getLemmaSiblingInCopy(fullCopy, easyLemma) shouldBe copyEasyLemma
    }

    @Disabled
    @Test
    fun getImports() {
    }


    @Disabled
    @Test
    fun setImports() {
    }


    @AfterEach
    fun cleanUp() {
        Paths.get(RENAME_DIR).toFile().listFiles().forEach { it.delete() }
        Paths.get(RENAME_DIR).toFile().delete()
    }


    companion object {
        private const val DIR = "src/test/resources/theoryfile"
        private const val RENAME_DIR = "$DIR/rename_test"
    }
}