/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.isabelle.theory

import io.kotlintest.data.forall
import io.kotlintest.shouldBe
import io.kotlintest.tables.row
import org.junit.jupiter.api.Test

internal class LemmaAnnotationTest {

    @Test
    fun `check text generation`() {
        val annotationString = "abc"
        val annotation = LemmaAnnotation(annotationString, null)
        annotation.text shouldBe listOf(annotationString)
    }

    @Test
    fun `check emptiness detection`() {
        forall(
            row(LemmaAnnotation("foo", null), false),
            row(LemmaAnnotation("", null), true)
        ) { annotation, expected -> annotation.empty shouldBe expected }
    }
}