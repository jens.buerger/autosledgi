/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.utils

import io.kotlintest.matchers.boolean.shouldBeTrue
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

internal class CoroutineCollectionUtilsKtTest {

    @Test
    fun `wait for multiple coroutines`() = runBlocking {
        val job1 = launch {
            delay(1005)
        }
        val job2 = launch {
            delay(1000)
        }
        val jobs = listOf(job1, job2)
        awaitAll(jobs)
        for (job in jobs) {
            job.isCompleted.shouldBeTrue()
        }
    }

    @Test
    fun `cancel multiple coroutines`() = runBlocking {
        val job1 = launch {
            delay(1005)
        }
        val job2 = launch {
            delay(1000)
        }
        val jobs = listOf(job1, job2)

        cancelAll(jobs)
        awaitAll(jobs) //wait until all jobs are really canceled
        for (job in jobs) {
            job.isCompleted.shouldBeTrue()
            job.isCancelled.shouldBeTrue()
        }
    }


}