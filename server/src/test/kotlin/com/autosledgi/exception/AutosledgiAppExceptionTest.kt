/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.exception

import io.kotlintest.shouldBe
import kotlinx.coroutines.CancellationException
import org.junit.Test

internal class AutosledgiAppExceptionTest {

    @Test
    fun `check exception message with null wrapped exception`() {
        val nestedException = IllegalArgumentException("wrong argument")
        val e = AutosledgiAppException("test", nestedException)
        e.cause shouldBe nestedException
        e.message shouldBe "test"
    }

    @Test
    fun `check exception hierarchy`() {
        var exceptionMarker = try {
            //throw java.lang.IllegalArgumentException()
            throw CancellationException()
        } catch (e: CancellationException) {
            // this is executed when the job is canceled
            0
        } catch (e: Exception) {
            1
        }

        exceptionMarker shouldBe 0

        exceptionMarker = try {
            throw java.lang.IllegalArgumentException()
        } catch (e: CancellationException) {
            0
        } catch (e: Exception) {
            1
        }

        exceptionMarker shouldBe 1

    }
}
