/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.cloudprovider.aws.s3

import com.autosledgi.common.utils.deleteFile
import com.autosledgi.common.utils.fileExists
import io.kotlintest.data.forall
import io.kotlintest.shouldBe
import io.kotlintest.tables.row
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import java.io.File
import java.net.URI
import java.nio.file.Paths

// TODO test this automatically with special bucket

@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
class S3FacadeTest {

    @Autowired
    lateinit var facade: S3Provider

    @Test
    fun `URI to string`() {
        forall(
            row("/bla"),
            row("s3://my-s3-bucket/foo/bar.zip"),
            row("s3://my-s3-bucket/**/a*.txt")
        ) { path -> URI(path).toString() shouldBe path }
    }

    @Test
    fun `URI hostname`() {
        forall(
            row("s3://a-bucket/bla.zip", "a-bucket"),
            row("s3://my-s3-bucket/foo/bar.zip", "my-s3-bucket")
        ) { fullPath, host -> URI(fullPath).host shouldBe host }
    }

    @Test
    fun `URI path`() {
        forall(
            row("s3://a-bucket/bla.zip", "/bla.zip"),
            row("s3://my-s3-bucket/foo/bar.zip", "/foo/bar.zip"),
            row("s3://my-s3-bucket/foo/*.zip", "/foo/*.zip")
        ) { fullPath, path -> URI(fullPath).path shouldBe path }
    }

    @Test
    fun `upload file to s3`() {
        facade.uploadFile(File(UPLOAD_FILE_PATH),
            UPLOAD_FILE_TARGET
        )
    }

    @Test
    fun `check path to string and vice versa`() {
        Paths.get("my-s3-bucket/foo/*.zip").toString() shouldBe "my-s3-bucket/foo/*.zip"
    }

    @Test
    fun bla() {
        val x = URI("file:///filename.txt")
        println(x.scheme)
    }


    @Test
    fun `download file from s3`() {
        facade.uploadFile(File(UPLOAD_FILE_PATH),
            UPLOAD_FILE_TARGET
        )
        val targetPath = Paths.get(UPLOAD_FILE_PATH + "_download")
        fileExists(targetPath) shouldBe false
        try {
            facade.downloadObject(UPLOAD_FILE_TARGET, targetPath)
            fileExists(targetPath) shouldBe true
        } finally {
            deleteFile(targetPath)
        }
    }


    @Test
    fun `delete file from s3`() {
        facade.deleteObject(UPLOAD_FILE_TARGET)
    }


    companion object {
        private const val TEST_DATA_DIR = "src/test/resources/genericS3Repo"
        private const val UPLOAD_FILE_PATH = "$TEST_DATA_DIR/test.txt"
        private val UPLOAD_FILE_TARGET = URI("s3://autosledgi.test/uploadtest/test.txt")
    }
}