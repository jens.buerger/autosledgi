/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.api.rest

import io.kotlintest.shouldNotBe
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.web.FilterChainProxy
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext


@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
internal class RESTTest {

    @Autowired
    lateinit var wac: WebApplicationContext

    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    lateinit var springSecurityFilterChain: FilterChainProxy

    private var _mockMvc: MockMvc? = null

    @Before
    fun setup() {
        this._mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
            .addFilter<DefaultMockMvcBuilder>(springSecurityFilterChain).build()
    }

    protected val mockMvc: MockMvc
        get() = _mockMvc!!


    protected fun isSecured(endpoint: String) =
        mockMvc.perform(get(endpoint)).andExpect(status().isUnauthorized)

    // TODO implement keycloak token getter


    @Test
    fun `mockmvc ready`() {
        _mockMvc shouldNotBe null
    }

}