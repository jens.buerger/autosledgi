/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.api.rest

import io.kotlintest.data.forall
import io.kotlintest.tables.row
import org.junit.Test


internal class JobResultDownloadControllerTest : RESTTest() {

    @Test
    fun `auth required for job result download endpoints`() {
        forall(
            row("${JOB_RESULT_DOWNLOAD_ENDPOINT}10")
        ) { isSecured(it) }
    }

    companion object {
        // hardcoded here on purpose to ensure standard compliance
        private const val JOB_RESULT_ENDPOINT = "/user/jobresource"
        private const val JOB_RESULT_DOWNLOAD_ENDPOINT = "$JOB_RESULT_ENDPOINT/"
    }
}