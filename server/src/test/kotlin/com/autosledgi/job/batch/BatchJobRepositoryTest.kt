/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.batch

import com.autosledgi.job.batch.jobwrapper.GenericBatchJob
import io.kotlintest.shouldBe
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import java.math.BigDecimal

// TODO test this automatically


@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
internal class BatchJobRepositoryTest {

    @Autowired
    lateinit var batchJobRepository: BatchJobRepository

    @Test
    fun `basic insert and load test`() {
        val job = GenericBatchJob()
        job.cost = BigDecimal("1.0")
        val jobSet = hashSetOf(job)
        batchJobRepository.save(job)
        jobSet.contains(job) shouldBe true

        job.command = listOf("1", "2").toTypedArray()
        job.environment["key"] = "val"
        batchJobRepository.save(job)
        batchJobRepository.findById(job.id!!).get() shouldBe job
    }

    //@Test


}