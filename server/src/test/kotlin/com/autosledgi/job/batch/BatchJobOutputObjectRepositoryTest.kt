/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.batch

import com.autosledgi.common.utils.*
import mu.KLogging
import org.junit.Ignore
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import java.io.ByteArrayInputStream
import java.nio.file.Paths
import java.util.zip.ZipEntry

@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
internal class BatchJobOutputObjectRepositoryTest {


    @Autowired
    lateinit var objectRepository: BatchJobOutputObjectRepository

    @Ignore
    @Test
    fun `unzip file from S3`() {
        val path = Paths.get("/1/result.zip")
        val fileName = "session_log2"
        processEntryFromZipInputStream(objectRepository.getInputStream(path), fileName) {
            val inputAsString = it.bufferedReader().readText()
            println(inputAsString)
        }
    }

    @Ignore
    @Test
    fun `test fancyZip`() {
        val pathIn = Paths.get("/1/result.zip")
        val pathOut = Paths.get("/1/resultFancy.zip")
        val streamIn = objectRepository.getInputStream(pathIn)
        val streamOut = objectRepository.getOutputStream(pathOut)
        zipFileMap(streamIn, streamOut) { entry: ZipEntryWithStream ->
            if (entry.entry.name == "session_log") {
                val newFileContent = "This is a String\n"
                val inputStream = ByteArrayInputStream(newFileContent.toByteArray(Charsets.UTF_8))
                ZipEntryWithStream(inputStream, ZipEntry(entry.entry.name)) //implicit return
            } else {
                entry //implicit return
            }
        }
    }

    @Ignore
    @Test
    fun `test zipAdd`() {
        val pathIn = Paths.get("/1/result.zip")
        val pathOut = Paths.get("/1/resultAdd.zip")
        val streamIn = objectRepository.getInputStream(pathIn)
        val streamOut = objectRepository.getOutputStream(pathOut)
        val newFileContent = "This is a String\n"
        val inputStream = ByteArrayInputStream(newFileContent.toByteArray(Charsets.UTF_8))
        val newEntry = ZipFileExtension(inputStream, Paths.get("/session_log3"))
        zipFileAdd(streamIn, streamOut, listOf(newEntry))
    }


    companion object : KLogging()
}