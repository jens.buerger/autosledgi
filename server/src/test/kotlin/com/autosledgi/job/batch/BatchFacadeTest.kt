/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.batch

import com.autosledgi.job.batch.jobwrapper.GenericBatchJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Ignore
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
internal class BatchFacadeTest {

    @Autowired
    lateinit var batchFacade: BatchFacade

    // TODO automate this test
    @Ignore
    @Test
    fun submitAndRun() = runBlocking<Unit> {
        val job = GenericBatchJob().also {
            it.cpuCores = 1
            it.memory = 256
            it.command = arrayOf("sh", "-c", "echo \$MYVAR && sleep 10")
            it.environment = mapOf("MYVAR" to "test").toMutableMap()
        }
        batchFacade.submitAndRun(job)

        launch {
            delay(9000)
            batchFacade.terminate(job)
        }

        // TODO reset database
        // we wait very long
        launch {
            repeat(40) {
                delay(2000)
            }
        }

    }
}