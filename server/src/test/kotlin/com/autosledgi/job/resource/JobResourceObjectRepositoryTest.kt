/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.resource

import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import java.io.File
import java.nio.file.Paths

@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
internal class JobResourceObjectRepositoryTest {

    @Autowired
    lateinit var objectRepository: JobResourceObjectRepository

    @Test
    fun `test resource bundle upload`() {
        objectRepository.uploadFile(
            File(UPLOAD_FILE_PATH),
            UPLOAD_FILE_TARGET
        )
    }

    companion object {
        private const val TEST_DATA_DIR = "src/test/resources/genericS3Repo"
        private const val UPLOAD_FILE_PATH = "$TEST_DATA_DIR/test.txt"
        private val UPLOAD_FILE_TARGET = Paths.get("/uploadtest2/test.txt")
    }
}