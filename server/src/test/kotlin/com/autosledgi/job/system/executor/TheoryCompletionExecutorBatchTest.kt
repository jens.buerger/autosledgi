/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system.executor


import com.autosledgi.job.batch.jobwrapper.IsabelleSessionBuildBatchJob
import com.autosledgi.job.batch.jobwrapper.IsabelleTheoProcBatchJob
import com.autosledgi.job.resource.JobResource
import com.autosledgi.job.system.JobPriority
import com.autosledgi.job.system.jobwrapper.TheoryCompletionJob
import com.autosledgi.job.system.jobwrapper.TheoryCompletionJobRequest
import com.autosledgi.user.KeycloakUserId
import com.autosledgi.user.UserServiceLevel
import kotlinx.coroutines.runBlocking
import mu.KLogging
import org.junit.Ignore
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import java.net.URI
import java.time.Duration

@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
@Transactional
internal class TheoryCompletionExecutorBatchTest  {

    @Autowired
    lateinit var theoryExecutor: TheoryCompletionSystemJobExecutor

    @Ignore
    @Test
    fun `TheoryCompletion Job Execution`() {
        val jobRequest = TheoryCompletionJobRequest(
            JobResource().also { it.uri = URI("s3://autosledgi.upload/jobResource/1/") },
            "eval",
            UserServiceLevel.GOLD,
            KeycloakUserId("test"))
        val job = TheoryCompletionJob(jobRequest).apply {
            addBatchJob(IsabelleSessionBuildBatchJob().also {
                it.cpuCores = 4
                it.memory = 8096
                it.command = arrayOf("isabelle_cloud_build.sh")
                it.environment = mapOf("CUSTOMER" to "debug",
                    "ISABELLE_VERSION" to "2018",
                    "CUSTOMER_ID" to "1",
                    "CPU_NUMBER" to "4",
                    "RUNNER_THEORY" to "eval",
                    "RUNNER_SESSION" to "fun").toMutableMap()
                it.timeout = Duration.ofMinutes(10)
                it.priority = JobPriority.PRICE_DRIVEN
            })
            addBatchJob(IsabelleTheoProcBatchJob().also{
                it.cpuCores = 4
                it.memory = 8096
                it.command = arrayOf("isabelle_cloud_build.sh")
                it.environment = mapOf("CUSTOMER" to "debug",
                    "ISABELLE_VERSION" to "2018",
                    "CUSTOMER_ID" to "1",
                    "CPU_NUMBER" to "4",
                    "RUNNER_THEORY" to "eval",
                    "RUNNER_SESSION" to "fun").toMutableMap()
                it.timeout = Duration.ofMinutes(10)
                it.priority = JobPriority.PRICE_DRIVEN
            })
            addBatchJob(IsabelleTheoProcBatchJob().also{
                it.cpuCores = 4
                it.memory = 8096
                it.command = arrayOf("isabelle_cloud_build.sh")
                it.environment = mapOf("CUSTOMER" to "debug",
                    "ISABELLE_VERSION" to "2018",
                    "CUSTOMER_ID" to "1",
                    "CPU_NUMBER" to "4",
                    "RUNNER_THEORY" to "eval",
                    "RUNNER_SESSION" to "fun").toMutableMap()
                it.timeout = Duration.ofMinutes(10)
                it.priority = JobPriority.PRICE_DRIVEN
            })
        }
        runBlocking{
            //val executed = launch { theoryExecutor.execute(job) }
            theoryExecutor.execute(job)
            //val initialDelay: Duration = Duration.ofMillis(45000)
            //logger.info { "Wait $initialDelay seconds" }
            //delay(initialDelay.toMillis())
            //logger.info { "Lets terminate" }
            //theoryExecutor.terminate(job)
            //executed.cancel()
        }
        logger.info{"Done"}
    }

    companion object : KLogging()
}

