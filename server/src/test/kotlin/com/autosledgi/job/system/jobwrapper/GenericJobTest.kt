/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system.jobwrapper

import io.kotlintest.shouldBe
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import java.time.Duration
import javax.transaction.Transactional

@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
@Transactional
internal class GenericJobTest {

    @Test
    fun `check constant parameter identifier compliance`() {
        // prevent unnoticed changes of constants
        INITIAL_DELAY_PARAM shouldBe "initial_delay"
    }

    @Test
    fun `check request translation`() {
        val request = GenericJobRequest(initialDelay = Duration.ofSeconds(10))
        val job = GenericJob(request)

        job.parameter.size shouldBe 1
        job.parameter.getValue(INITIAL_DELAY_PARAM).toLong() shouldBe request.initialDelay!!.toMillis()
    }
}