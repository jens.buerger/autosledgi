/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system

import com.autosledgi.job.batch.jobwrapper.GenericBatchJob
import com.autosledgi.job.resource.JobResource
import com.autosledgi.job.system.jobwrapper.*
import com.autosledgi.user.KeycloakUserId
import com.autosledgi.user.UserServiceLevel
import io.kotlintest.shouldBe
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import java.net.URI


@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
@Transactional
internal class SystemJobRepositoryTest {

    @Autowired
    lateinit var repository: SystemJobRepository

    @Test
    fun `basic insert test`() {
        val autosledgiJob = SystemJob()
        repository.save(autosledgiJob)
    }

    @Test
    fun `Type 1 insert test with child batch job`() {
        val batchJob = GenericBatchJob()
        val autosledgiJob = SystemJob()
        repository.save(autosledgiJob)
        autosledgiJob.addBatchJob(batchJob)
        repository.save(autosledgiJob)
    }

    @Test
    fun `Type 2 insert test with child batch job`() {
        val batchJob = GenericBatchJob()
        val autosledgiJob = SystemJob()
        autosledgiJob.addBatchJob(batchJob)
        repository.save(autosledgiJob)
    }

    @Test
    fun `save and load theory completion job`() {
        val theoryCompletionJob =
            TheoryCompletionJob(
                TheoryCompletionJobRequest(
                    JobResource().also { it.uri = URI("s3://sample.bucket/") },
                    "mainTheo",
                    UserServiceLevel.PLATINUM,
                    KeycloakUserId("none")
                )
            )
        repository.save(theoryCompletionJob)

        (repository.findById(theoryCompletionJob.id!!).get() is TheoryCompletionJob) shouldBe true
    }

    @Test
    fun `save and load generic job`() {
        val job = GenericJob(GenericJobRequest())
        repository.save(job)

        (repository.findById(job.id!!).get() is GenericJob) shouldBe true
    }

    @Test
    fun `save and load maintenance job`() {
        val job = MaintenanceJob()
        repository.save(job)

        (repository.findById(job.id!!).get() is MaintenanceJob) shouldBe true
    }


}