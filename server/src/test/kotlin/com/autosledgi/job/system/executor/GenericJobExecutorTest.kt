/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system.executor

import com.autosledgi.job.system.jobwrapper.GenericJob
import com.autosledgi.job.system.jobwrapper.GenericJobRequest
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.transaction.annotation.Transactional
import java.time.Duration
import java.time.Instant

@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
@Transactional
internal class GenericJobExecutorTest {

    @Autowired
    lateinit var genericJobExecutor: GenericJobExecutor

    @Test
    fun `generic job termination`() {
        val job = GenericJob(DELAY_JOB_REQ)
        runBlocking {
            genericJobExecutor.terminate(job)
        }
    }

    @Test
    fun `check delay param effectiveness`() {
        val startTime = Instant.now()
        runBlocking {
            genericJobExecutor.execute(GenericJob(DELAY_JOB_REQ))
        }
        val endTime = Instant.now()
        Duration.between(startTime, endTime) > Duration.ofSeconds(1)
    }


    companion object {
        val DELAY_JOB_REQ = GenericJobRequest(initialDelay = Duration.ofSeconds(2))
    }
}