/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system

import com.autosledgi.exception.AutosledgiJobException
import com.autosledgi.job.system.jobwrapper.GenericJob
import com.autosledgi.job.system.jobwrapper.GenericJobRequest
import io.kotlintest.matchers.collections.shouldContain
import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import io.kotlintest.shouldThrow
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import java.time.Duration
import javax.transaction.Transactional

@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
@Transactional
internal class SystemJobFacadeTest {

    @Autowired
    lateinit var jobFacade: SystemJobFacade

    @Autowired
    lateinit var jobRepository: SystemJobRepository


    @Test
    fun `launch simple job without complications`() = runBlocking {
        val job = GenericJob(SHORT_DELAY_JOB_REQ)

        job.status shouldBe JobStatus.CREATED
        val coroutine = jobFacade.launch(job)

        // JOB IS RUNNING
        listOf(JobStatus.RUNNING, JobStatus.SUCCEEDED).shouldContain(job.status)

        coroutine.join() // wait for job

        // JOB FINISHED
        job.status shouldBe JobStatus.SUCCEEDED
        job.runtime shouldNotBe null

        // termination of finished job should fail
        shouldThrow<AutosledgiJobException> {
            jobFacade.terminate(job)
        }
        // job results should be stored in DB
        jobRepository.findById(job.id!!).get() shouldBe job
    }

    @Test
    fun `terminate long running job`() = runBlocking {
        val job = GenericJob(LONG_DELAY_JOB_REQ)

        job.status shouldBe JobStatus.CREATED
        jobFacade.launch(job)

        job.status shouldBe JobStatus.RUNNING

        jobFacade.terminate(job).join() //call terminate and wait for it


        job.status shouldBe JobStatus.TERMINATED
        (job.runtime!! < LONG_DELAY_JOB_REQ.initialDelay) shouldBe true

        // second termination of terminated job should fail
        shouldThrow<AutosledgiJobException> {
            jobFacade.terminate(job)
        }

        jobRepository.findById(job.id!!).get() shouldBe job
    }

    @Test
    fun `retrieve saved job from facade via id`() {
        val job = GenericJob(SHORT_DELAY_JOB_REQ)
        jobRepository.save(job)

        jobFacade.getJobById(job.id!!).get() shouldBe job
    }

    companion object {
        val SHORT_DELAY_JOB_REQ = GenericJobRequest(initialDelay = Duration.ofSeconds(1))
        val LONG_DELAY_JOB_REQ = GenericJobRequest(initialDelay = Duration.ofSeconds(10))
    }
}