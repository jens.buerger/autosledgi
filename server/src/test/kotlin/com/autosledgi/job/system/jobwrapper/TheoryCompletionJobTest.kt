/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.job.system.jobwrapper

import com.autosledgi.job.resource.JobResource
import com.autosledgi.user.KeycloakUserId
import com.autosledgi.user.UserServiceLevel
import io.kotlintest.shouldBe
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import java.net.URI

@RunWith(SpringRunner::class)
@ExtendWith(SpringExtension::class)
@SpringBootTest
internal class TheoryCompletionJobTest {

    @Test
    fun `check constant parameter identifier compliance`() {
        // prevent unnoticed changes of constants
        MAIN_THEORY_JOB_PARAM shouldBe "main_theory"
        SERVICE_LEVEL_JOB_PARAM shouldBe "service_level"
    }

    @Test
    fun `check request translation`() {
        val job = TheoryCompletionJob(JOB_REQ1)

        job.parameter.size shouldBe 2
        job.parameter[MAIN_THEORY_JOB_PARAM] shouldBe JOB_REQ1.mainTheoryName
        job.parameter[SERVICE_LEVEL_JOB_PARAM] shouldBe JOB_REQ1.serviceLevel.toString()
        job.userId shouldBe JOB_REQ1.userId
        job.resource shouldBe JOB_REQ1.resource
    }


    companion object {
        private val JOB_REQ1 = TheoryCompletionJobRequest(
            JobResource().also { it.uri = URI("s3://sample.bukect/") },
            "Scratch",
            UserServiceLevel.GOLD,
            KeycloakUserId("test")
        )
    }
}