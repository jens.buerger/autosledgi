# AutoSledgi - ATP in the Cloud Framework (Public Version)

### Dependencies

* JDK 8
* Kotlin 1.3+
* PostgreSQL 10+
* Keycloak (c.f. server/deploy for the exact setup)

### Tools

* Intellij IDEA
* Gradle
* SpringBoot
* FlyWay


### Development Setup

More details on how to configure your development environment can be found in the Wiki.
