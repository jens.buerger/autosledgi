/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.textfile

import io.kotlintest.shouldBe
import org.junit.Ignore
import org.junit.jupiter.api.Test

import java.nio.file.Paths

internal class TextFileTest {

    companion object {
        private const val testFile1Path: String = "src/test/resources/textfile/test1.txt"
        private const val testFile2Path: String = "src/test/resources/textfile/test2.txt"
        private val refContents1 = listOf("Line 0", "Line 1", "Line 2")
        private val refContents2 = listOf("Line 5", "Line 6", "Line 7")
    }


    private var textFile1: TextFile
    private var textFile2: TextFile

    init {
        textFile1 = TextFile(Paths.get(testFile1Path))
        textFile2 = TextFile(Paths.get(testFile2Path))
    }

    @Test
    fun `test get contents`() {
        textFile1.contents shouldBe refContents1
        textFile2.contents shouldBe refContents2
    }

    @Ignore
    @Test
    fun setContents() {
    }

    @Test
    fun `get Path`() {
        textFile1.path shouldBe Paths.get(testFile1Path)
        textFile2.path shouldBe Paths.get(testFile2Path)
    }


    @Test
    fun `append line on non-empty file`() {
        val line = "foo"
        textFile1.appendLine(line)
        textFile1.contents[textFile1.contents.size - 1] shouldBe line
    }

    @Test
    fun `append line on empty file`() {
        val textFile = TextFile()
        textFile.appendLine("foo")
        textFile.contents shouldBe listOf("foo")
    }

    @Test
    fun `append lines on non-empty file`() {
        val lines = listOf("foo1", "foo2")
        textFile1.appendLines(lines)
        textFile1.contents.subList(3, 5) shouldBe lines
    }

    @Test
    fun `insertion of a single line`() {
        val line = "foo"
        textFile1.insertLine(1, line)
        textFile1.contents.subList(0, 3) shouldBe listOf(refContents1[0], "foo", refContents1[1])
    }

    @Test
    fun `insertion of multiple lines`() {
        val lines = listOf("foo1", "foo2")
        textFile1.insertLines(1, lines)
        textFile1.contents.subList(0, 4) shouldBe listOf(refContents1[0], "foo1", "foo2", refContents1[1])
    }

    @Test
    fun `set a single line`() {
        val line = "foo"
        textFile1.setLine(0, line)
        textFile1.contents.subList(0, 2) shouldBe listOf("foo", refContents1[1])
    }

    @Test
    fun `set lines withing existing lines`() {
        val lines = listOf("foo1", "foo2")
        textFile1.setLines(1, lines)
        textFile1.contents.subList(1, 1 + lines.size) shouldBe lines
    }

    @Test
    fun `set lines out of range`() {
        val lines = listOf("foo1", "foo2")
        val originalSize = textFile1.contents.size
        val index = textFile1.contents.size + 1
        textFile1.setLines(index, lines)
        textFile1.contents.subList(index, index + lines.size) shouldBe lines
        textFile1.contents[originalSize] shouldBe textFile1.lineFiller
    }

    @Test
    fun `delete a single line`() {
        textFile1.deleteLine(0)
        textFile1.contents shouldBe refContents1.slice(1 until refContents1.size)
    }

    @Test
    fun `delete multiple lines`() {
        textFile1.deleteLines(1..2)
        textFile1.contents[0] shouldBe refContents1[0]
        textFile1.contents.size shouldBe 1
    }

    @Ignore
    @Test
    fun writeBack() {
    }

    @Ignore
    @Test
    fun copyToFile() {
    }

    @Ignore
    @Test
    fun updateFromFS() {
    }
}