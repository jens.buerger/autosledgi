/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.utils

import com.autosledgi.common.textfile.TextFile.FilePosition
import com.autosledgi.common.textfile.TextFile.FileRange
import io.kotlintest.shouldBe

import org.junit.jupiter.api.Test
import java.util.regex.Pattern


class StringUtilsTest {

    @Test
    fun `test List of String extension firstCharOf`() {
        val firstName = "someName"
        val secondName = "otherName"

        val list1 = listOf(
            """ def thisIsNotIt:: "bla" where "lala" """,
            """ lemma $firstName: "Das ist ein lemma" """,
            """ apply simp """,
            """ done  lemma $secondName:""",
            """   "a=b" """,
            """ by simp  """)

        // Erster Treffer
        list1.findNthMatch(Pattern.compile(
            """(?:\(\*OPTIONS>(.*?)\*\).*?|)"""
                    + """(?:lemma|theorem) (?:(\S+)\s*(\[.*?\]\s*?|):|(\[.*?\]\s*?|))(.*?)(?=proof|apply|by)(.*?)(?=end|lemma|theorem|\Z)""",
            Pattern.DOTALL
        ),1) shouldBe
                FileRange(FilePosition(1,1), FilePosition(3, 7))

        // Spezieller Name
        list1.findNthMatch(Pattern.compile(
            """(?:\(\*OPTIONS>(.*?)\*\).*?|)"""
                    + """(?:lemma|theorem) (?:($secondName)\s*(\[.*?\]\s*?|):|(\[.*?\]\s*?|))\s*(".*?")\s*(?=proof|apply|by)(.*?)(?=end|lemma|theorem|\Z)""",
            Pattern.DOTALL
        ),1) shouldBe
                FileRange(FilePosition(3,7), FilePosition(5, 10))

        // Zweiter Treffer
        list1.findNthMatch(Pattern.compile(
            """(?:\(\*OPTIONS>(.*?)\*\).*?|)"""
                    + """(?:lemma|theorem) (?:(\S+)\s*(\[.*?\]\s*?|):|(\[.*?\]\s*?|))(.*?)(?=proof|apply|by)(.*?)(?=end|lemma|theorem|\Z)""",
            Pattern.DOTALL
        ),2) shouldBe
                FileRange(FilePosition(3,7), FilePosition(5, 10))
    }


    @Test
    fun test() {
        System.out.println("Hallo\nDas\nIch\n".lines().size)
    }
}