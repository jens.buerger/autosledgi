/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.utils

import io.kotlintest.data.forall
import io.kotlintest.shouldBe


import io.kotlintest.tables.row
import org.junit.jupiter.api.Test


class CollectionUtilsTest {

    @Test
    fun `test intRange subList extension on nonempty list`() {
        val list1 = listOf(0, 1, 2, 3, 4)
        forall(
            row(list1, IntRange(1, 2), listOf(1, 2)),
            row(list1, IntRange(1, 1), listOf(1))
        ) { list, indexRange, res -> list.immutableSubListFromRange(indexRange) shouldBe res }
    }

    @Test
    fun `test intRange subList extension on empty range`() {
        listOf(0, 1, 2, 3, 4).subList(0, 0) shouldBe listOf()
    }


    // TODO perform tests for subListFromRange
}