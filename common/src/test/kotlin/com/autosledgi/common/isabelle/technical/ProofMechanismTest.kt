/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.isabelle.technical

import io.kotlintest.shouldBe
import org.junit.jupiter.api.Test

internal class ProofMechanismTest {

    @Test
    fun `parse proof mechanism from simple string`() {
        for (mechanism in ProofMechanism.values()) {
            if (mechanism == ProofMechanism.ISAR) {
                ProofMechanism.parseProofMechanism("proof -") shouldBe setOf(mechanism)
            } else {
                ProofMechanism.parseProofMechanism("foo\n apply( ${mechanism.str_name})") shouldBe setOf(mechanism)
            }

        }
    }

    @Test
    fun `parse combined proof mechanisms from string`() {
        val input = """proof -
            |show ?thesis
            |apply (simp add: cont2cont)
            |qed
        """.trimMargin()
        ProofMechanism.parseProofMechanism(input) shouldBe setOf(ProofMechanism.ISAR, ProofMechanism.SIMP)
    }
}