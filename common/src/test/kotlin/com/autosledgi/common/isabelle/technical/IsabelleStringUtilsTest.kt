/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.isabelle.technical

import io.kotlintest.data.forall
import io.kotlintest.shouldBe
import io.kotlintest.tables.row
import org.junit.jupiter.api.Test

internal class IsabelleStringUtilsTest {

    @Test
    fun `remove isabelle comments from string`() {
        forall(
            row("", ""),
            row("(**)", ""),
            row("(* *)test", "test"),
            row("text{* *}", ""),
            row("(* *) test (* *)", " test ")
        ) { str, expected -> str.removeIsabelleComments() shouldBe expected }
    }

    @Test
    fun `remove isabelle comments from list`() {
        forall(
            row(listOf("(**)", "(* *)test"), listOf("", "test")),
            row(emptyList(), emptyList())
        ) { str, expected -> str.removeIsabelleComments() shouldBe expected }
    }

    @Test
    fun `normalize string for isabelle`() {
        forall(
            row("(* *)test", "test"),
            row("text{* *}", ""),
            row("(* *)    test (* *)", "test"),
            row("a\n\nb", "a\nb")
        ) { str, expected -> str.isabelleNormalize() shouldBe expected }
    }

    @Test
    fun `normalize string list for isabelle`() {
        forall(
            row(listOf("(* *)test"), listOf("test")),
            row(listOf("(* *)test\n", "\n", "\n", "blub"), listOf("test", "blub"))
        ) { str, expected -> str.isabelleNormalize() shouldBe expected }
    }
}