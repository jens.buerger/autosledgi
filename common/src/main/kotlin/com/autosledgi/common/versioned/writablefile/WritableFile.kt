/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.versioned.writablefile

import java.io.OutputStream
import java.nio.file.Path

/**
 * Interface for writable files.
 *
 * @param C The type of writable file content representation.
 */
interface WritableFile<C> {

    var path: Path?
    var contents: C


    /**
     * Update the contents property from the file system
     */
    fun updateFromFS()

    /**
     * Write the writable file contents back to the writable file system
     */
    fun writeBack()

    /**
     * Copy the writable file contents to another writable file.
     * Also makes sure that writable file contents in the copy are adapted if necessary.
     *
     * @param path The path of the copy.
     */
    fun copyToFile(path: Path)

    /**
     * Copy the file contents to an output stream.
     *
     * @param outputStream The output stream, the content should be pushed to.
     */
    fun copyToOutputStream(outputStream: OutputStream)

    /**
     * Move the writable file contents to another location.
     *
     * @param path The path to the new location.
     */
    fun moveToFile(path: Path)

}