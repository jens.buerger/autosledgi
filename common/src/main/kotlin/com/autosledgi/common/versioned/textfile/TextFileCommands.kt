/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.versioned.textfile

import com.autosledgi.common.textfile.TextFile
import com.autosledgi.common.utils.concatLines
import com.autosledgi.common.utils.immutableSubListFromRange
import com.autosledgi.common.utils.strictCheckIndexRange
import com.autosledgi.common.utils.subList
import com.autosledgi.common.versioned.writablefile.FileCommand
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Class for TextFile commands
 *
 * @param textFile The TextFile that is modified by the com.autosledgi.common.command
 * @author Jens Buerger
 */
abstract class TextFileCommand(private val textFile: TextFile) : FileCommand<MutableList<String>>(textFile) {

    var contentsCache: List<String>? = null


    override fun rollbackFailedExecution() {
        this.textFile.contents = contentsCache?.toMutableList() ?: throw IllegalStateException("Rollback not possible")
    }
}


class InsertLinesCommand(
    private val textFile: TextFile,
    private val startIndex: Int,
    private val newLines: List<String>
) : TextFileCommand(textFile) {

    init {
        textFile.contents.strictCheckIndexRange(startIndex until startIndex + newLines.size)
    }


    override fun execute() {
        contentsCache = textFile.contents.toList()
        textFile.insertLines(startIndex, newLines)
        contentsCache = null
    }


    override fun undo() {
        textFile.contents.subList(startIndex until startIndex + newLines.size).clear()
    }
}

class SetLinesCommand(
    private val textFile: TextFile,
    private val startIndex: Int,
    private val newLines: List<String>
) : TextFileCommand(textFile) {

    private var oldLines: List<String>? = null
    private val affectedIndices: IntRange = startIndex until startIndex + newLines.size


    init {
        textFile.contents.strictCheckIndexRange(affectedIndices)
    }


    override fun execute() {
        contentsCache = textFile.contents.toList()
        oldLines = textFile.contents.subList(affectedIndices).toList()
        textFile.setLines(startIndex, newLines)
        contentsCache = null
    }

    override fun undo() {
        textFile.deleteLines(affectedIndices)
        oldLines?.let { textFile.insertLines(startIndex, it) }
            ?: throw IllegalStateException("Cannot undo before execution")
    }
}

class DeleteLinesCommand(
    private val textFile: TextFile,
    private val lineRange: IntRange
) : TextFileCommand(textFile) {

    private var deletedLines: List<String>? = null

    init {
        textFile.contents.strictCheckIndexRange(lineRange)
    }

    override fun execute() {
        contentsCache = textFile.contents.toList()
        deletedLines = textFile.contents.immutableSubListFromRange(lineRange).toList()
        textFile.deleteLines(lineRange)
        contentsCache = null
    }

    override fun undo(): Unit = deletedLines?.let { textFile.insertLines(lineRange.start, it) }
        ?: throw IllegalStateException("Cannot undo before execution")


}

class ReplaceFirstMatchCommand(
    private val textFile: TextFile,
    private val pattern: Pattern,
    private val replacement: String
) : TextFileCommand(textFile) {

    private var replacedString: String? = null

    override fun execute() {
        contentsCache = textFile.contents.toList()
        val matcher: Matcher = pattern.matcher(textFile.contents.concatLines())
        if (matcher.find()) {
            replacedString = matcher.group(0)
            textFile.replaceFirstMatch(pattern, replacement)
        } else {
            throw IllegalArgumentException("Pattern not present in the file")
        }
        contentsCache = null
    }

    override fun undo() {
        replacedString?.let { textFile.replaceFirstMatch(Pattern.compile(Pattern.quote(replacement)), it) }
            ?: throw IllegalStateException("Cannot undo before execution")

    }

}