/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.versioned.textfile

import com.autosledgi.common.textfile.TextFile
import com.autosledgi.common.textfile.WritableTextFile
import com.autosledgi.common.versioned.writablefile.VerWritableFile
import java.io.OutputStream
import java.nio.charset.Charset
import java.nio.file.Path
import java.util.regex.Pattern

/**
 * Class for com.autosledgi.common.versioned text files
 *
 * @property textFile The text file that should be com.autosledgi.common.versioned.
 * @author Jens Buerger
 */
open class VerTextFile(private val textFile: TextFile) :
    VerWritableFile<MutableList<String>>(textFile),
    WritableTextFile {

    // explicit delegation of WritableTextFile properties required
    override var fileCharset: Charset
        get() = textFile.fileCharset
        set(value) {
            textFile.fileCharset = value
        }

    override var lineFiller: String
        get() = textFile.lineFiller
        set(value) {
            textFile.lineFiller = value
        }

    override var contents: MutableList<String>
        get() = textFile.contents
        set(value) {
            textFile.contents = value
        }


    /**
     * @param path The file path.
     * @constructor Load text file from the file system.
     */
    constructor(path: Path) : this(TextFile(path))


    /**
     * Create a empty text file that is not backed by an actual file on the file system.
     */
    constructor() : this(TextFile())


    /**
     * Append a line to the end of the text file.
     *
     * @param line The line that should be appended.
     * @return The new com.autosledgi.common.command state.
     */
    fun appendLine(line: String) = storeAndExecute(
        InsertLinesCommand(
            textFile,
            textFile.size,
            listOf(line)
        )
    )


    /**
     * Append lines to the end of the text file.
     *
     * @param lines List of lines that should be appended.
     * @return The new com.autosledgi.common.command state.
     */
    fun appendLines(lines: List<String>) = storeAndExecute(
        InsertLinesCommand(
            textFile,
            textFile.size,
            lines
        )
    )


    /**
     * Insert a line into the text file. Lines with index > lineIndex are moved.
     *
     * @param lineIndex The line index.
     * @param line      The line that should be inserted.
     * @return The new com.autosledgi.common.command state.
     */
    fun insertLine(lineIndex: Int, line: String) =
        storeAndExecute(InsertLinesCommand(textFile, lineIndex, listOf(line)))


    /**
     * Insert lines into the text file. Lines after the inserted lines are moved.
     *
     * @param startIndex The start index of the insertion.
     * @param lines      The lines that should be inserted.
     * @return The new com.autosledgi.common.command state.
     */
    fun insertLines(startIndex: Int, lines: List<String>) =
        storeAndExecute(InsertLinesCommand(textFile, startIndex, lines))


    /**
     * Set the contents of a single line.
     * If startIndex is not present, lines in between are filled.
     *
     * @param lineIndex The index of the line that should be overwritten.
     * @param line      The new line contents.
     * @return The new com.autosledgi.common.command state.
     */
    fun setLine(lineIndex: Int, line: String) = storeAndExecute(
        SetLinesCommand(
            textFile,
            lineIndex,
            listOf(line)
        )
    )


    /**
     * Set the contents of lines.
     * If startIndex is not present, lines in between are filled.
     *
     * @param startIndex The start index of the overwrite.
     * @param lines      The lines contents.
     * @return The new com.autosledgi.common.command state.
     */
    fun setLines(startIndex: Int, lines: List<String>) = storeAndExecute(
        SetLinesCommand(
            textFile,
            startIndex,
            lines
        )
    )


    /**
     * Delete a line from the text file.
     *
     * @param lineIndex The index of the line that should be deleted.
     * @return The new com.autosledgi.common.command state.
     */
    fun deleteLine(lineIndex: Int) = storeAndExecute(
        DeleteLinesCommand(
            textFile,
            lineIndex..lineIndex
        )
    )


    /**
     * Delete multiple consecutive lines from the text file.
     *
     * @param lineRange The range of lines that should be deleted.
     * @return The new com.autosledgi.common.command state.
     */
    fun deleteLines(lineRange: IntRange) = storeAndExecute(DeleteLinesCommand(textFile, lineRange))


    /**
     * Replace the first match of the pattern with the replacement string.
     *
     * Note, the com.autosledgi.common.textfile is converted into a single string before the replacements takes place.
     *
     * @param pattern The pattern that should be replaced.
     * @param replacement The replacement string.
     */
    fun replaceFirstMatch(pattern: Pattern, replacement: String) =
        storeAndExecute(ReplaceFirstMatchCommand(textFile, pattern, replacement))

    override fun copyToFile(path: Path) {
        textFile.copyToFile(path)
    }

    override fun copyToOutputStream(outputStream: OutputStream) {
        textFile.copyToOutputStream(outputStream)
    }

}