/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.versioned.writablefile

/**
 * Class for com.autosledgi.common.versioned writable files based on the com.autosledgi.common.command pattern
 *
 * @param C The content type of the com.autosledgi.common.versioned writable file.
 * @property file The backing writable file.
 * @property invoker The com.autosledgi.common.command invoker.
 */
open class VerWritableFile<C>(
    protected val file: WritableFile<C>,
    private val invoker: com.autosledgi.common.command.CommandInvoker<FileCommand<C>> = com.autosledgi.common.command.CommandInvoker()
) : com.autosledgi.common.command.Versioned<FileCommand<C>> by invoker, WritableFile<C> by file {

    override val redoPossible: Boolean
        get() = invoker.redoPossible

    override val undoPossible: Boolean
        get() = invoker.undoPossible
}

