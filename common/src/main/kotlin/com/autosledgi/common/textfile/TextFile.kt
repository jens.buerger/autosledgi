/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.textfile


import com.autosledgi.common.utils.*
import mu.KLogging
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.regex.Pattern

private val DEFAULT_CHARSET = Charset.forName("UTF-8")!!

/**
 * Class for text files.
 *
 *
 * Note: Changes are only written to the writable file system when the corresponding
 * write methods are called.
 *
 * @author Jens Buerger
 */
class TextFile : WritableTextFile {

    override var contents: MutableList<String> = ArrayList()
    override var path: Path? = null
    override var fileCharset = DEFAULT_CHARSET
    override var lineFiller = ""

    data class FilePosition(var lineNr: Int, var colNr: Int)
    data class FileRange(var start: FilePosition, var end: FilePosition)

    companion object : KLogging()

    /**
     * Create a new TextFile from Scratch.
     *
     * Note that the file is not backed by an actual file in the file system without
     * providing a correct path
     */
    constructor() {
        path = null
        contents = ArrayList()
    }

    constructor(content: List<String>) : this() {
        contents = ArrayList(content)
    }

    /**
     * Create a new TextFile object based on an existing writable file.
     *
     * @param path The path of the existing writable file.
     */
    constructor(path: Path) {
        this.path = path
        contents = ArrayList(Files.readAllLines(path, fileCharset))
    }

    /**
     * Create a new TextFile object based on an input stream
     *
     * @param inputStream The input stream that should be used
     * @note Does not close the stream
     */
    constructor(inputStream: InputStream, charset: Charset = DEFAULT_CHARSET) {
        inputStream.use {
            contents = ArrayList(splitLines(it.bufferedReader(charset).readText()))
        }
    }

    /**
     * Append a line to the end of the text file.
     *
     * @param line The line that should be appended.
     * @return True iff the append operation was successful.
     */
    fun appendLine(line: String) {
        contents.add(line)
    }

    /**
     * Append lines to the end of the text file.
     *
     * @param lines List of lines that should be appended.
     * @return True iff the append operation was successful.
     */
    fun appendLines(lines: List<String>) {
        contents.addAll(lines)
    }


    /**
     * Insert a line into the text file. Lines with index > lineIndex are moved.
     *
     * @param lineIndex The line index.
     * @param line      The line that should be inserted.
     */
    fun insertLine(lineIndex: Int, line: String) {
        contents.add(lineIndex, line)
    }


    /**
     * Insert lines into the text file. Lines after the inserted lines are moved.
     *
     * @param startIndex The start index of the insertion.
     * @param lines      The lines that should be inserted.
     */
    fun insertLines(startIndex: Int, lines: List<String>) {
        contents.addAll(startIndex, lines)
    }


    /**
     * Set the contents of a single line.
     * If startIndex is not present, lines in between are filled.
     *
     * @param lineIndex The index of the line that should be overwritten.
     * @param line      The new line contents.
     */
    fun setLine(lineIndex: Int, line: String) {
        for (it in contents.size..lineIndex) {
            contents.add(lineFiller)
        }
        contents[lineIndex] = line
    }

    /**
     * Set the contents of lines.
     * If startIndex is not present, lines in between are filled.
     *
     * @param startIndex The start index of the overwrite.
     * @param lines      The lines contents.
     */
    fun setLines(startIndex: Int, lines: List<String>) {
        for (it in contents.size..startIndex + 1) {
            contents.add(lineFiller)
        }
        (0 until lines.size).forEach { i -> contents[startIndex + i] = lines[i] }
    }


    /**
     * Delete a line from the text file.
     *
     * @param lineIndex The index of the line that should be deleted.
     * @return The line that has been deleted.
     */
    fun deleteLine(lineIndex: Int): String = contents.removeAt(lineIndex)


    /**
     * Delete multiple consecutive lines from the text file.
     *
     * @param lineRange The range of lines that should be deleted.
     */
    fun deleteLines(lineRange: IntRange) = contents.subList(lineRange).clear()

    /**
     * Replace the first match of the pattern with the replacement string.
     *
     * Note, the com.autosledgi.common.textfile is converted into a single string before the replacements takes place.
     *
     * @param pattern The pattern that should be replaced.
     * @param replacement The replacement string.
     */
    fun replaceFirstMatch(pattern: Pattern, replacement: String) {
        contents = ArrayList(contents.replaceFirstMatch(pattern, replacement))
    }

    /**
     * Replace the n-th match of the pattern with the replacement string.
     *
     * Note, the com.autosledgi.common.textfile is converted into a single string before the replacements takes place.
     *
     * @param pattern The pattern that should be replaced.
     * @param index The index of the pattern to be replaced (i.e., "n")
     * @param replacement The replacement string.
     */
    fun replaceNthMatch(pattern: Pattern, index: Int, replacement: String): List<String> =
        contents.replaceNthMatch(pattern, index, replacement)

    // The position of the first matched character
    //fun firstMatchOf(pattern: Pattern):FilePosition = contents.firstCharOf(pattern)

    override fun writeBack() = path?.let { copyToFile(it) } ?: throw IllegalStateException("Path unset")

    override fun copyToFile(path: Path) {
        logger.info { "Writing ${contents.size} lines to ${path.fileName}" }
        Files.write(path, contents, fileCharset)
    }

    override fun copyToOutputStream(outputStream: OutputStream) =
        outputStream.use { it.write(contents.concatLines().toByteArray(charset = fileCharset)) }

    fun getInputStream(): InputStream = ByteArrayInputStream(contents.concatLines().toByteArray(charset = fileCharset))

    override fun moveToFile(path: Path) {
        logger.info { "Moving ${this.path} to $path" }
        writeBack()
        Files.move(this.path, path)
        this.path = path
    }

    override fun updateFromFS() = path?.let { contents = ArrayList(Files.readAllLines(path, fileCharset)) }
        ?: throw java.lang.IllegalStateException("Path unset")

    fun getDeepCopy(): TextFile {
        return TextFile(contents).also { copy ->
            copy.path = path?.let { Paths.get(it.toUri()) }
            copy.lineFiller = lineFiller
            copy.fileCharset = Charset.forName(fileCharset.name())
        }
    }

}
