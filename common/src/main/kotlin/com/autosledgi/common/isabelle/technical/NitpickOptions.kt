/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.isabelle.technical

import java.time.Duration


/**
 * Representation of Nitpick options.
 *
 * More information on Nitpick can be found here: http://com.autosledgi.common.isabelle.in.tum.de/dist/doc/nitpick.pdf
 *
 * @property timeout The timeout for the Nitpick operation.
 * @author Jens Buerger
 */
data class NitpickOptions(val timeout: Duration?) {

    val command: String
        // check out http://isabelle.in.tum.de/dist/doc/nitpick.pdf Section 5
        get() = TODO("implement the nitpick generator")
}