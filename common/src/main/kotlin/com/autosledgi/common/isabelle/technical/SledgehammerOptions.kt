/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.isabelle.technical

import java.time.Duration

data class SledgehammerOptions(
    val timeout: Duration?,
    val allowedATProvers: Set<ATProvers>?,
    val max_facts: Int?,
    val isar_proofs: Boolean = true
) {

    val command: String
        // see https://isabelle.in.tum.de/doc/sledgehammer.pdf
        get() {
            val options: MutableList<String> = ArrayList()
            timeout?.let { options.add("timeout = ${it.seconds}") }
            options.add("isar_proofs=$isar_proofs")
            allowedATProvers?.let { set -> "provers=${set.joinToString { it.str_name }}" }
            max_facts?.let { options.add("max_facts=$it") }
            return """sledgehammer${options.joinToString(",", prefix = "[", postfix = "]")}"""
        }

    constructor() : this(null, null, null)
}

