/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.isabelle.technical

import java.util.regex.Pattern


/**
 * Enumeration for the different ATP providers integrated in sledgehammer.
 *
 * @property str_name The string representation of the proof mechanism.
 * @property pattern The regex pattern that can be used to find the proof mechanism. in strings/console output.
 * @author Jens Buerger
 */
enum class ProofMechanism(val str_name: String, val pattern: Pattern, val desirability: Int) {

    SIMP("simp", Pattern.compile("simp"), 0),
    AUTO("auto", Pattern.compile("auto"), 0),
    FORCE("force", Pattern.compile("(?<!fast)force"), 2),
    FASTFORCE("fastforce", Pattern.compile("fastforce"), 2),
    METIS("metis", Pattern.compile("metis"), 2),
    SMT("smt", Pattern.compile("smt"), 99),
    SORRY("sorry", Pattern.compile("""(?:^|\s)(?:sorry|\\<proof>)"""), 100),
    OOPS("oops", Pattern.compile("""(?:^|\s)oops"""), 100),
    ISAR("isar", Pattern.compile("proof"), 1);
    // TODO add better pattern for OTHER mechanism
    //OTHER("other", Pattern.compile(".*"));


    companion object {
        /**
         * Search for proof mechanism in a string.
         *
         * @param line The line that contains the proof mechanism
         * @return The ATP Provider or null if no ATP provider was found
         */
        fun parseProofMechanism(line: String): Set<ProofMechanism> {
            val mechanisms = HashSet<ProofMechanism>()
            for (proofMechanism in ProofMechanism.values()) {
                if (proofMechanism.pattern.matcher(line).find()) {
                    mechanisms += proofMechanism
                }
            }
            return mechanisms
        }
    }

}