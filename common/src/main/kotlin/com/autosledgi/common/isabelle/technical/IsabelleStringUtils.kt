/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.isabelle.technical

import com.autosledgi.common.utils.EMPTY_STRING
import com.autosledgi.common.utils.removeEmptyLines
import java.util.regex.Pattern
import java.util.regex.Pattern.DOTALL

private val COMMENT_PATTERN = Pattern.compile("""\(\*.*?\*\)|text\s*?\{\*.*?}""", DOTALL)
private val DUPL_VERTICAL_WHITESPACE = Pattern.compile("""\v+(?=\v)""")
private val DUPL_HORIZONTAL_WHITESPACE = Pattern.compile("""\v+(?=\v)""")
const val DEF_INDENT = "    "

/**
 * Remove all comments from a char sequence.
 *
 * @return The filtered string.
 */
fun String.removeIsabelleComments(): String = COMMENT_PATTERN.matcher(this).replaceAll("")


/**
 * Remove all comments from a string list.
 *
 * @return The filtered string list.
 */
fun List<String>.removeIsabelleComments(): List<String> = this.map { it.removeIsabelleComments() }


/**
 * Normalize a string such that for Isabelle they are the same.
 *
 * @return The normalized char sequence.
 */
fun String.isabelleNormalize(): String {
    val cleanedLines = DUPL_VERTICAL_WHITESPACE.matcher(removeIsabelleComments()).replaceAll(EMPTY_STRING).trim()
    return DUPL_HORIZONTAL_WHITESPACE.matcher(cleanedLines).replaceAll(EMPTY_STRING)
}


/**
 * Normalize a string list such that for com.autosledgi.common.isabelle they are the same.
 *
 * @return The normalized char sequence.
 */
fun List<String>.isabelleNormalize(): List<String> = this.removeEmptyLines().map { it.isabelleNormalize().trim() }


