/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.isabelle.technical

import java.util.regex.Pattern

/**
 * Enumeration for the different ATP providers integrated in sledgehammer.
 *
 * @property str_name The string representation of the ATP provider.
 * @property pattern The regex pattern that can be used to find the ATP Name in strings/console output.
 * @author Jens Buerger
 */
enum class ATProvers(val str_name: String, private val pattern: Pattern) {
    CVC4("cvc4", Pattern.compile("cvc4")),
    Z3("z3", Pattern.compile("z3")),
    SPASS("spass", Pattern.compile("spass")),
    E("e", Pattern.compile("(?<![a-zA-Z])e")),
    REMOTE_VAMPIRE("remote_vampire", Pattern.compile("remote_vampire"));

    companion object {
        /**
         * Search for ATP providers in a string.
         *
         * @param line The line that contains the atp name.
         * @return The ATP Provider or null if no ATP provider was found
         */
        fun parseATPProvider(line: String): com.autosledgi.common.isabelle.technical.ATProvers? {
            for (prover in com.autosledgi.common.isabelle.technical.ATProvers.values()) {
                if (prover.pattern.matcher(line).find()) {
                    return prover
                }
            }
            return null
        }
    }

}