/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.utils


/**
 * Check if index is in the bound of the list, otherwise throw an exception
 *
 * @param index The index that should be checked.
 * @throws IllegalArgumentException if the index is not in bounds of the list.
 */
fun <T> Collection<T>.strictCheckIndex(index: Int) {
    if (index !in this.indices) {
        throw IllegalArgumentException("Index out of bounds")
    }
}

/**
 * Check if index range is in the bound of the list, otherwise throw an exception
 *
 * @param indexRange The index range that should be checked.
 * @throws IllegalArgumentException if the index range is not in bounds of the list.
 */
fun <T> Collection<T>.strictCheckIndexRange(indexRange: IntRange) {
    if (indexRange.start !in this.indices || indexRange.endInclusive !in this.indices) {
        throw IllegalArgumentException("Index Range out of bounds")
    }
}

fun <E> List<E>.immutableSubListFromRange(intRange: IntRange): List<E> =
    if (intRange.isEmpty()) emptyList() else subList(intRange.start, intRange.endInclusive + 1)

fun <E> MutableList<E>.subList(intRange: IntRange): MutableList<E> =
    if (intRange.isEmpty()) ArrayList() else subList(intRange.start, intRange.endInclusive + 1)