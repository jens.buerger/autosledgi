/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.utils

import java.io.*
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.attribute.FileTime
import java.security.MessageDigest
import java.time.Instant
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream


fun fileExists(path: Path): Boolean = with(File(path.toString())) { exists() && !isDirectory }

fun deleteFile(path: Path): Boolean = File(path.toString()).delete()

infix fun Path.renameFile(name: String): Path = Paths.get(parent.toString(), name)

/**
 * Process file with fileName from a inputStream which represents a zip File.
 *
 * @param inputStream The stream for the zip file.
 * @param fileName The filename in the zip.
 * @param process The function that is executed on the stream of fileName.
 *
 * @Note Process function may not close the stream - this is done automatically!!!
 */
fun processEntryFromZipInputStream(
    inputStream: InputStream,
    fileName: String,
    process: (ZipInputStream) -> Unit
) {
    inputStream.use { objectStream ->
        val zipIn = ZipInputStream(objectStream).also { zipIn ->
            var entry: ZipEntry? = null
            while ({ entry = zipIn.nextEntry;entry }() != null) {
                try {
                    if (entry!!.name == fileName) {
                        process(zipIn)
                        break
                    }
                } catch (e: Exception) {
                    throw e
                }
            }
        }
        consumeRestOfStream(objectStream)
        zipIn.close()
    }
}

fun processAllEntriesFromZipInputStream(
    inputStream: InputStream,
    process: (ZipEntryWithStream) -> Unit
) {
    inputStream.use { objectStream ->
        val zipIn = ZipInputStream(objectStream).also { zipIn ->
            var entry: ZipEntry? = null
            while ({ entry = zipIn.nextEntry;entry }() != null) {
                process(ZipEntryWithStream(zipIn, entry!!))
            }
        }
        consumeRestOfStream(objectStream)
        zipIn.close()
    }
}

fun hashAllZipEntries(inputStream: InputStream): Map<ZipEntry, ByteArray> {
    val digest = MessageDigest.getInstance("SHA-256")
    val hashes = HashMap<ZipEntry, ByteArray>()
    processAllEntriesFromZipInputStream(inputStream) {
        val byteArrayOutputStream = ByteArrayOutputStream()
        it.inputStream.copyTo(byteArrayOutputStream)
        hashes[it.entry] = digest.digest(byteArrayOutputStream.toByteArray())
    }
    return hashes
}

fun bytesToHex(hash: ByteArray): String =
    hash.joinToString("") { String.format("%02X", (it.toInt() and 0xFF)) }

/**
 * Transform every entry in a zip file from input stream according to the mapFun function and push it to an output stream
 *
 * @param inputStream The input stream of the zip file.
 * @param outputStream The output stream of the zip file.
 * @param mapFun The transformation function for the entries
 *
 * @note if mapFun returns null, the corresponding input entry is deleted in the output
 */
fun zipFileMap(
    inputStream: InputStream, outputStream: OutputStream,
    mapFun: (ZipEntryWithStream) -> ZipEntryWithStream? = { it }
) {
    transformZipStreams(inputStream, outputStream) { zipInputStream: ZipInputStream,
                                                     zipOutputStream: ZipOutputStream ->
        zipFileMapInner(zipInputStream, zipOutputStream, mapFun)
    }
}

/**
 * Add files from the additionalFiles list to the zip and push it to an output stream
 *
 * @param inputStream The input stream of the zip file.
 * @param outputStream The output stream of the zip file.
 * @param  additionalFiles TThe list of files that should be added to the zip file.
 *
 * @note if mapFun returns null, the corresponding input entry is deleted in the output
 */
fun zipFileAdd(inputStream: InputStream, outputStream: OutputStream, additionalFiles: List<ZipFileExtension>) {
    transformZipStreams(inputStream, outputStream) { zipInputStream: ZipInputStream,
                                                     zipOutputStream: ZipOutputStream ->
        zipFileAddInner(zipInputStream, zipOutputStream, additionalFiles)
    }
}

private fun zipFileMapInner(
    zipInputStream: ZipInputStream, zipOutputStream: ZipOutputStream,
    process: (ZipEntryWithStream) -> ZipEntryWithStream?
) {
    var entry: ZipEntry? = null
    while ({ entry = zipInputStream.nextEntry;entry }() != null) {
        val transformedEntry = process(ZipEntryWithStream(zipInputStream, entry!!))
        transformedEntry?.let {
            addInputStreamToZipFile(it, zipOutputStream)
        }
    }
}

private fun zipFileAddInner(
    zipInputStream: ZipInputStream, zipOutputStream: ZipOutputStream, additionalFiles: List<ZipFileExtension>
) {
    var entry: ZipEntry? = null
    while ({ entry = zipInputStream.nextEntry;entry }() != null) {
        val transformedEntry = ZipEntryWithStream(zipInputStream, entry!!)
        addInputStreamToZipFile(transformedEntry, zipOutputStream)
    }
    for (file in additionalFiles) {
        val newEntry = ZipEntry(file.path.toString()).also {
            it.creationTime = (FileTime.from(Instant.now()))
        }
        val entryWithStream = ZipEntryWithStream(file.inputStream, newEntry)
        addInputStreamToZipFile(entryWithStream, zipOutputStream)
    }
}

fun entriesToZipStream(entries: List<ZipEntryWithStream>, zipOut: OutputStream) {
    zipOut.use { outStream ->
        ZipOutputStream(outStream).use { zipOutputStream ->
            for (entryWithStream in entries) {
                val newEntry = entryWithStream.entry.also {
                    it.creationTime = (FileTime.from(Instant.now()))
                }
                addInputStreamToZipFile(ZipEntryWithStream(entryWithStream.inputStream, newEntry), zipOutputStream)
            }
            zipOutputStream.finish()
        }
    }
}

private fun addInputStreamToZipFile(
    entry: ZipEntryWithStream, zipOut: ZipOutputStream
) {
    zipOut.putNextEntry(entry.entry)
    entry.inputStream.copyTo(zipOut)
    zipOut.closeEntry()
}

private fun transformZipStreams(
    inputStream: InputStream, outputStream: OutputStream,
    transformer: (ZipInputStream, ZipOutputStream) -> Unit
) {
    outputStream.use { outStream ->
        ZipOutputStream(outStream).use { zipOut ->
            inputStream.use { inStream ->
                val zipIn = ZipInputStream(inStream).also {
                    transformer(it, zipOut)
                }
                consumeRestOfStream(inStream)
                zipIn.close()
            }
            zipOut.finish()
            zipOut.close()
        }
    }
}

private fun consumeRestOfStream(stream: InputStream) {
    while (stream.read() >= 0) {
        // read stream to end to avoid S3 warning
    }
}


fun cloneInputStream(inputStream: InputStream): InputStream {

    val byteOutputStream = ByteArrayOutputStream()
    inputStream.use { input ->
        byteOutputStream.use { output ->
            input.copyTo(output)
        }
    }
    return ByteArrayInputStream(byteOutputStream.toByteArray())
}



data class ZipEntryWithStream(val inputStream: InputStream, val entry: ZipEntry)
data class ZipFileExtension(val inputStream: InputStream, val path: Path)
