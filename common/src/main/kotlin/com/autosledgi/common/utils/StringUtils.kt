/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.utils

import com.autosledgi.common.textfile.TextFile
import java.util.regex.Matcher
import java.util.regex.Pattern

const val NEWLINE_CHAR = "\n"
const val EMPTY_STRING = ""
private val REGEX_META_PATTERN: Pattern = Pattern.compile("""(?<!\\)\((?!\?)""")


/**
 * Get the outer pattern for a provided pattern.
 *
 * @return A pattern that matches the complete pattern, but has no subgroups.
 */
fun Pattern.outerPattern(): Pattern {
    val matcher: Matcher = REGEX_META_PATTERN.matcher(this.toString())
    return if (matcher.find()) {
        Pattern.compile(String.format("(%s)", matcher.replaceAll("(?:")), Pattern.DOTALL)
    } else {
        Pattern.compile(String.format("(%s)", this.toString()))
    }
}


/**
 * Concatenate multiple lines into a single string separated by a newline char.
 *
 * @return The concatenated string.
 */
fun List<String>.concatLines(): String = this.joinToString(separator = NEWLINE_CHAR)


/**
 * Split lines string into multiple lines list.
 *
 * @return The input list with all the lines.
 */
fun splitLines(lines: String): List<String> = lines.split(NEWLINE_CHAR)


/**
 * Trim all lines in a string list.
 *
 * @return The input list with all the lines trimmed.
 */
fun List<String>.trimAll(): List<String> = this.map(String::trim)


/**
 * Trim a string list.
 *
 * Note that in contrast to trimAll() this function only trims the list start and endings
 *
 * @return The input list with all the lines trimmed.
 */
fun List<String>.trim(): List<String> = splitLines(concatLines().trim())


/**
 * Left trim all lines in a string list.
 *
 * @return The input list with all the lines left trimmed.
 */
fun List<String>.lTrimAllLines(): List<String> = this.map { it.replaceFirst("""^\s+""", EMPTY_STRING) }


/**
 * Remove empty lines (their trimmed repentant is empty) from a string list.
 *
 * @return The input list without empty lines.
 */
fun List<String>.removeEmptyLines(): List<String> = this.filter { it.trim() != EMPTY_STRING }


/**
 * Add a string prefix to all strings in a string list.
 *
 * @return The input list where every list element is prefixed.
 */
fun List<String>.addPrefix(prefix: String): List<String> = this.map { prefix + it }


/**
 * Get the number of pattern matches.
 *
 * @return The number of matches.
 */
fun String.numOfMatches(pattern: Pattern): Int {
    var counter = 0
    val matcher: Matcher = pattern.matcher(this)
    while (matcher.find()) {
        counter++
    }
    return counter
}


/**
 * Get the number of pattern matches.
 *
 * Note: Lines are viewed independently!
 *
 * @return The number of matches.
 */
fun List<String>.numOfMatches(pattern: Pattern): Int {
    var counter = 0
    val matcher = pattern.matcher("")
    for (line in this) {
        matcher.reset(line)
        while (matcher.find()) {
            counter++
        }
    }
    return counter
}


fun List<String>.replaceFirstMatch(pattern: Pattern, replacement: String): List<String> {
    val oldText = concatLines()
    val matcher = pattern.matcher(oldText)
    return if (matcher.find()) {
        splitLines(oldText.replaceRange(matcher.start(), matcher.end(), replacement))
    } else {
        toList()
    }
}



fun List<String>.replaceNthMatch(pattern: Pattern, index: Int, replacement: String): List<String> {
    val concatLines: String = concatLines()
    val matcher: Matcher = pattern.matcher(concatLines)
    var curMatches = 0

    while (matcher.find()) {
        curMatches++
        if (curMatches == index) {
            return splitLines(
                concatLines.substring(0, matcher.start()) + replacement +
                        if (matcher.end() < concatLines.length) concatLines.substring(matcher.end()) else EMPTY_STRING
            )
        }
    }

    throw IndexOutOfBoundsException("pattern not found")
}


/**
 * Finds the position of the n-th match of the pattern. The position is expressed using a range of positions.
 * Each position can modeled as a "cursor", i.e., it sits between two characters. All indices are zero-based
 *
 * Example: In the following string, the position of "Santa\nClaus" is ((0,13), (1,5))
 *   "Jingle Bells Santa"
 *   "Claus is a LIE    "
 */
fun List<String>.findNthMatch(pattern: Pattern, index: Int): TextFile.FileRange {
    var startLine = 0
    var startCol = 0
    var endLine = 0
    var endCol = 0

    var str = concatLines()
    var matcher = pattern.matcher(str)

    var found = false
    for(i in 1..index) {
        found = matcher.find()
    }

    if(found) {
        startLine = str.substring(0, matcher.start()).lines().size-1
        endLine = str.substring(0, matcher.end()).lines().size-1

        str = this.subList(startLine, endLine+1).concatLines()
        matcher = pattern.matcher(str)
        matcher.find() // Always true
        startCol = matcher.start()
        val endOffsetWithNewlines = matcher.end()
        val newlineChars = endLine - startLine
        val endOffsetWithoutNewlines = endOffsetWithNewlines - newlineChars
        val cummLengthOfPrevLines = this.subList(startLine, endLine).joinToString(separator = "").length
        endCol = endOffsetWithoutNewlines - cummLengthOfPrevLines
    }

    return TextFile.FileRange(TextFile.FilePosition(startLine,startCol),TextFile.FilePosition(endLine,endCol))
}