/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.command

/**
 * Exception that is thrown if a com.autosledgi.common.command execution failed
 *
 * @property message The exception message.
 * @property cause The exception cause.
 * @author Jens Buerger
 */
class CommandExecutionException(message: String?, cause: Throwable?) : Exception(message, cause) {
    constructor(cause: Throwable?) : this(null, cause)
}