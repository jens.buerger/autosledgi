/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.command

/**
 * Represents a lightweight state of the com.autosledgi.common.command invoker.
 *
 * @property index The com.autosledgi.common.command queue index at the invoker.
 * @property command The com.autosledgi.common.command that was executed when this state was created.
 * @author Jens Buerger
 */
data class CommandState<T : com.autosledgi.common.command.Command>(val index: Int, val command: T?)