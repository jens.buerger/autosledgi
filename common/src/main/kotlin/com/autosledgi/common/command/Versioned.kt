/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.command

/**
 * Interface for CommandInvoker Delegation.
 */
interface Versioned<T : com.autosledgi.common.command.Command> {

    /**
     * There exists at least one com.autosledgi.common.command that can be redone.
     */
    val redoPossible: Boolean


    /**
     * There exists at least one com.autosledgi.common.command that can be undone.
     *
     */
    val undoPossible: Boolean


    /**
     * Get the current com.autosledgi.common.command state
     *
     * @return The current com.autosledgi.common.command state
     */
    fun getCurrentState(): com.autosledgi.common.command.CommandState<T>

    /**
     * Execute [command] and store it in the com.autosledgi.common.command queue.
     *
     * @param command The com.autosledgi.common.command that should be executed.
     *
     * @return The new com.autosledgi.common.command state.
     */
    fun storeAndExecute(command: T): com.autosledgi.common.command.CommandState<T>


    /**
     * Undo the latest action that has been undone.
     *
     * @return The new com.autosledgi.common.command state.
     * @throws IllegalStateException If redo is not possible.
     */
    fun undo(): com.autosledgi.common.command.CommandState<T>


    /**
     * Redo the last action that has been undone.
     *
     * @throws IllegalStateException If redo is not possible.
     * @return The new com.autosledgi.common.command state.
     */
    fun redo(): com.autosledgi.common.command.CommandState<T>


    /**
     * Go to the latest version (execute all undone changes).
     *
     * @return The new com.autosledgi.common.command state.
     */
    fun goToLatestVersion(): com.autosledgi.common.command.CommandState<T>


    /**
     * Go to a specific version.
     *
     * @param commandState The com.autosledgi.common.command state that should be restored.
     * @throws IllegalArgumentException If the state cannot be restored.
     */
    fun goToVersion(commandState: com.autosledgi.common.command.CommandState<T>)


    /**
     * Check if the current com.autosledgi.common.command state is equal to the current one.
     *
     * @param commandState The com.autosledgi.common.command state that should be checked.
     * @return True iff the provided com.autosledgi.common.command state is equal to the current one.
     */
    fun isCurrentState(commandState: com.autosledgi.common.command.CommandState<T>): Boolean


    /**
     * Check if a com.autosledgi.common.command state is restorable.
     *
     * @param commandState The state that should be checked.
     * @return true if state can be restored, false otherwise.
     */
    fun isStateRestorable(commandState: com.autosledgi.common.command.CommandState<T>): Boolean

}