/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.command

/**
 * Invokes commands and stores their history
 *
 * @author Jens Buerger
 */
class CommandInvoker<T : com.autosledgi.common.command.Command> : com.autosledgi.common.command.Versioned<T> {

    private val commands: MutableList<T> = ArrayList()
    private var currentIndex: Int = -1

    override val redoPossible: Boolean
        get() = currentIndex + 1 < commands.size

    override val undoPossible: Boolean
        get() = currentIndex >= 0


    override fun getCurrentState(): com.autosledgi.common.command.CommandState<T> =
        if (currentIndex < 0) {
            com.autosledgi.common.command.CommandState(currentIndex, null)
        } else {
            com.autosledgi.common.command.CommandState(currentIndex, commands[currentIndex])
        }


    override fun storeAndExecute(command: T): com.autosledgi.common.command.CommandState<T> {
        // execute first to provoke exceptions
        try {
            command.execute()
        } catch (e: Exception) {
            command.rollbackFailedExecution()
            throw CommandExecutionException(e)
        }

        if (redoPossible) {
            commands.subList(currentIndex + 1, commands.size).clear()
        }
        commands.add(command)
        currentIndex++
        return CommandState(currentIndex, command)
    }


    @Synchronized
    override fun undo(): com.autosledgi.common.command.CommandState<T> {
        if (undoPossible) {
            commands[currentIndex].undo()
            currentIndex--
            return getCurrentState()
        } else {
            throw IllegalStateException("Cannot undo further")
        }
    }


    @Synchronized
    override fun redo(): com.autosledgi.common.command.CommandState<T> {
        if (redoPossible) {
            commands[currentIndex + 1].execute()
            currentIndex++
            return getCurrentState()
        } else {
            throw IllegalStateException("Cannot redo further")
        }
    }


    @Synchronized
    override fun goToLatestVersion(): com.autosledgi.common.command.CommandState<T> {
        while (redoPossible) {
            redo()
        }
        return getCurrentState()
    }


    override fun goToVersion(commandState: com.autosledgi.common.command.CommandState<T>) {
        if (!isStateRestorable(commandState)) {
            throw java.lang.IllegalArgumentException("State not in com.autosledgi.common.command queue")
        }

        while (currentIndex > commandState.index) {
            undo()
        }

        while (currentIndex < commandState.index) {
            redo()
        }
    }


    override fun isCurrentState(commandState: com.autosledgi.common.command.CommandState<T>): Boolean =
        commandState == getCurrentState()


    override fun isStateRestorable(commandState: com.autosledgi.common.command.CommandState<T>): Boolean =
        commandState.index == -1 ||
                (commandState.index in commands.indices && commands[commandState.index] == commandState.command)

}