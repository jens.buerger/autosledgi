/*
 * Copyright 2019 AutoSledgi Contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.autosledgi.common.command

/**
 * Interface for a com.autosledgi.common.command
 *
 * @author Jens Buerger
 */
interface Command {

    /**
     * Perform the action.
     */
    fun execute()

    /**
     * Undo the action.
     */
    fun undo()

    /**
     * Fixes a failed execution i.e. an exception was thrown during execute().
     *
     * Unlike undo this even works if the execution did not completely went through.
     * After a successful execution this function does not need to work.
     *
     * Note: This function is automatically called by the invoker if an exception was thrown during execute()
     *
     */
    fun rollbackFailedExecution()
}